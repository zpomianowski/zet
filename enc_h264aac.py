import os
import sys
import subprocess
import threading
import re
import time
from md5generator import md5gen
from constants import *

""" 
Module, to proper work needs:
Encoder:
__init__(self, parent, id, film)
start(self)
stop(self)
clear(self)
"""

class Encoder:
    def __init__(self, parent, id, film):
        self.hP = None
        self.bat = None
        self.parent = parent             
        self.film = film 
        self.id = id
        ## diagnostic parameters
        self.dia_video = 0
        self.dia_audio = 0
        self.dia_mp4box = 0 
        self.dia_ap = 0 
        ## control
        self.encstatus = 0
                
    def start(self):
        if self.film['thumb'] == "no thumb":
            sys.stderr.write("ERR: No thumbnail!\n")
            #self.encstatus = -1
        if self.encstatus < 0:
            return self.encstatus

        info = "Please wait..." 
        
        ##computing md5 checksum
        md5 = md5gen(self.film['path'])
        md5.start()

        ## video
        self.bat = BATENCV(self, self.film) 
        
        if self.encstatus < 0:
            return self.encstatus
              
        print self.getTime() + " ENC: Encoding video..."      
        cmd = ".bat"        
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, env=os.environ, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        x264 = x264monitor(self.film)        
        while self.hP.poll() is None:
            string = self.hP.stdout.read(100)
            #sys.stderr.write(string)
            self.dia_video, info = x264.getInfo(string)
            ## set diagnostics info            
            total = 0.9*self.dia_video+0.09*self.dia_audio+0.01*self.dia_mp4box
            progress = "Progress: %0.2f%s - %s" % (total, "%", info)
            self.parent.parent.parent.SetTitle(progress) 
            self.parent.parent.parent.SetStatusText(progress)            
            self.parent.gauge_task.SetValue(total)
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Fatal error - couldn't encode video\n")
            self.encstatus = -1
            return self.encstatus
        if self.encstatus == 1:
            return 1
        self.dia_video = 100
        
        ## audio
        self.bat = BATENCA(self.film)   
            
        print self.getTime() + " ENC: Encoding audio..."      
        cmd = ".bat"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        nero = neromonitor()        
        while self.hP.poll() is None: 
            string = self.hP.stdout.read(100)                           
            self.dia_audio, info = nero.getInfo(string)
            #sys.stderr.write(string + "\n")
            ## set diagnistics info            
            total = 0.9*self.dia_video+0.09*self.dia_audio+0.01*self.dia_mp4box
            progress = "Progress: %0.2f%s - %s" % (total, "%", info)
            self.parent.parent.parent.SetTitle(progress) 
            self.parent.parent.parent.SetStatusText(progress)            
            self.parent.gauge_task.SetValue(total)
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Fatal error - couldn't encode audio\n")
            self.encstatus = -2
            return self.encstatus
        if self.encstatus == 1:
            return 1
        self.dia_audio = 100
        
        ## muxing
        self.bat = BATENCM(self.film) 
          
        print self.getTime() + " ENC: Muxing video and audio, creating ISO file..."      
        cmd = ".bat"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        mp4box = mp4boxmonitor()        
        while self.hP.poll() is None: 
            string = self.hP.stdout.read(1)                           
            info = mp4box.getInfo(string)                   
            ## set diagnistics info            
            total = 0.9*self.dia_video+0.09*self.dia_audio+0.01*self.dia_mp4box
            progress = "Progress: %0.2f%s - %s" % (total, "%", info)
            self.parent.parent.parent.SetTitle(progress) 
            self.parent.parent.parent.SetStatusText(progress)  
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Fatal error - couldn't mux video and audio\n")
            self.encstatus = -3
            return self.encstatus
        if self.encstatus == 1:
            return 1
        self.parent.gauge_task.SetValue(100)
       
        ## tagging        
        md5.join()
        self.bat = BATTAG(self.film, md5.value)
              
        print self.getTime() + " ENC: Tagging process..."  
        time.sleep(1)    
        cmd = ".bat"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        ap = apmonitor()    
        while self.hP.poll() is None: 
            string = self.hP.stdout.read(100) 
            info = ap.getInfo(string)
            self.parent.parent.parent.SetTitle("Progress: 100% - Writing metadata...") 
            self.parent.parent.parent.SetStatusText("Progress: 100% - Writing metadata...")
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Error - couldn't write tags into container\n")
            self.encstatus = -4
            return self.encstatus
        if self.encstatus == 1:
            return 1
            
        ## finishing
        timestring = str(time.localtime()[0]) + "." + \
                    str(time.localtime()[1]).zfill(2) + "." + \
                    str(time.localtime()[2]).zfill(2) + " [" + \
                    str(time.localtime()[3]).zfill(2) + ":" + \
                    str(time.localtime()[4]).zfill(2) + ":" + \
                    str(time.localtime()[5]).zfill(2) + "]"             
        self.parent.parent.parent.SetTitle("Progress: 100% - Finished - " + timestring) 
        self.parent.parent.parent.SetStatusText("Progress: 100% - Finished - " + timestring) 
        print self.getTime() + " ENC: Done"         
        
        return self.encstatus  
    
    def getTime(self):
        timestring = "[" + str(time.localtime()[0]) + "." + \
                        str(time.localtime()[1]).zfill(2) + "." + \
                        str(time.localtime()[2]).zfill(2) + " " + \
                        str(time.localtime()[3]).zfill(2) + ":" + \
                        str(time.localtime()[4]).zfill(2) + ":" + \
                        str(time.localtime()[5]).zfill(2) + "]"    
        return timestring
        
    def stop(self):
        self.encstatus = 1  
        if self.hP != None:
            #try:
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW                
                handle = subprocess.Popen("taskkill /F /T /PID %i" % self.hP.pid , shell=False, startupinfo=startupinfo)
                handle.wait()
                print self.getTime() + " ENC: Process terminated!"
            #except Exception:
            #    sys.stderr.write(self.getTime() + " ENC: Error - couldn'terminate - Windows sucks;]\n")
         
    def clear(self):
        return
        time.sleep(1)
        try:
            if os.path.exists('.bat'):
                os.remove('.bat')
            if os.path.exists('.avs'):
                os.remove('.avs')
            if os.path.exists('output\\.stats.temp'):
                os.remove('output\\.stats.temp')
            if os.path.exists('output\\.stats'):
                os.remove('output\\.stats')
            film = "output\\%s.mp4" % (self.film['name'])    
            video = "output\\%s_video.mp4" % (self.film['name'])
            audio = "output\\%s_audio.mp4" % (self.film['name'])
            if os.path.exists(film):
                if os.path.exists(video):
                    os.remove(video)
                if os.path.exists(audio):
                    os.remove(audio)
            else:
                sys.stderr.write("ENC: Error - no result file found\n")
            print self.getTime() + " ENC: Unneeded files cleaned"
        except Exception:
            if self.encstatus != -6:
                sys.stderr.write(self.getTime() + " ENC: Error - couldn't delete trash files\n")
            
class x264monitor:
    def __init__(self, film):
        self.parser = re.compile(r'\[(\d{,3}.\d)%]\s(\d+/\d+\sframes),\s(\d+\.\d{2}\sfps),\s(\d+\.\d{,2}\skb/s),\s(eta\s\d+:\d{2}:\d{2})')
        self.etaparser = re.compile(r'eta\s(\d+):(\d+):(\d+)')    
        self.p = 0
        self.tempp = 0
        self.f = "0 frames"
        self.fps = "0 fps"
        self.kbps = "0 kb/s"
        self.eta = "Not available"
        self.passes = None
        self.currentpass = 1        
        if film['profile'] != PROFILES[3] and film['profile'] != PROFILES[5] and film['profile'] != PROFILES[6]:
            self.passes = 2
        else:
            self.passes = 1
    
    def getInfo(self, string):        
        info = "Please wait..."       
        dia_v = 0
        tup = self.parser.search(string)                 
        if tup:
            self.p = float(tup.group(1))
            self.f = tup.group(2)
            self.fps = tup.group(3)
            self.kbps = tup.group(4)
            self.eta = tup.group(5)
        if self.passes == 1:
            dia_v = self.p                         
        if self.passes == 2:            
            if self.currentpass == 1:                
                dia_v = self.p/2
                self.get2xEta(str(self.eta)) 
                if self.p < self.tempp:
                    self.currentpass = 2
                self.tempp = self.p
            if self.currentpass == 2:
                dia_v = 50+self.p/2                
        info = \
            "Video %0.1f%s Pass:%i/%i" % (dia_v, "%", self.currentpass, self.passes) + \
            " [%s, %s, %s, %s]" % (self.f, self.fps, self.kbps, self.eta)      
        return dia_v, info
   
    def get2xEta(self, eta):
        tup = self.etaparser.search(eta)
        h = 0
        m = 0
        s = 0
        if tup:
            h = int(tup.group(1))
            m = int(tup.group(2))
            s = int(tup.group(3))
        #s *= 2
        #m *= 2
        #h *= 2
        if s > 59:
            m += 1
            s -= 60
        if m > 59:
            h += 1
            m -= 60
        self.eta = "eta %s:%s:%s - 1st pass" % (str(h), str(m).zfill(2), str(s).zfill(2))

class neromonitor:
    def __init__(self):
        self.parser = re.compile(r'(\d{,2},\d{13})%')
        self.p = 0
        self.dia_a = 0
    
    def getInfo(self, string):        
        info = "Please wait..."              
        tup = self.parser.search(string)                         
        if tup:            
            self.p = float(re.sub(r',', r'.', tup.group(1)))
            if self.p > self.dia_a:
                self.dia_a = self.p            
        info = "Audio: %i%s" % (self.dia_a, "%")        
        return self.dia_a, info
    
class mp4boxmonitor:
    def __init__(self):
        self.parser = re.compile(r'(ISO\sFile\sWriting:\s\|=*\s*\|)')
        self.p = 0
    
    def getInfo(self, string):        
        info = "Please wait..." 
        dia_a = "|"      
        tup = self.parser.search(string)                         
        if tup:            
            self.p = tup.group(1)
            dia_a = self.p
            info = "Bulding file: %s" % (self.p)        
        return info

class apmonitor:
    def __init__(self):
        self.parser = re.compile(r'Updating\smetadata.{3}\s+completed')
        self.p = 0
    
    def getInfo(self, string):
        info = "Please wait..."             
        tup = self.parser.search(string)                         
        if tup:            
            self.p = tup.group(0)
            print "%s" % str(self.p)            
            info = "Writing metadata..."        
        return info
                    
class AVS:
    def __init__(self, film):
        ##format searcher
        self.filter = re.compile(r"(.+?)(\.([^\.]*))?$")
        format = self.filter.search(film['path']).group(3).lower()
        
        ##aspectratio autodetect
        if film['aspectr'] == ASPECTR[0]:
            self.autodetectDAR(film)
        self.autodetectCODEC(film)
        self.autodetectINTERLACEMENT(film)
        
        ##settings depended on PROFILES             
        path = film['path'] 
        size = None
        #MQ
        if film['profile'] == PROFILES[4]:
            if film['aspectr'] == ASPECTR[2]:
                size = (448, 336)
            elif film['aspectr'] == ASPECTR[1]:
                size = (592, 336)
        # preview
        elif film['profile'] == PROFILES[5]:
            if film['aspectr'] == ASPECTR[2]:
                size = (768, 576)
            elif film['aspectr'] == ASPECTR[1]:
                size = (1024, 576)
            else:               
                size = (1280, 544)
        #fullHD
        elif film['profile'] == PROFILES[0]:
            if film['aspectr'] == ASPECTR[2]:
                size = (1440, 1080)
            elif film['aspectr'] == ASPECTR[1]:
                size = (1920, 1080)
        #HD
        elif film['profile'] == PROFILES[1]:            
            if film['aspectr'] == ASPECTR[2]:
                size = (960, 720)
            elif film['aspectr'] == ASPECTR[1]:
                size = (1280, 720)
        #SD YT
        elif film['profile'] == PROFILES[2] or film['profile'] == PROFILES[3]:            
            if film['aspectr'] == ASPECTR[2]:
                size = (768, 576)
            elif film['aspectr'] == ASPECTR[1]:
                size = (1024, 576)
        if not size:
            if film['profile'] == PROFILES[4]:                
                size = (592, 336)
            elif film['profile'] == PROFILES[5]:
                size = (1024, 576)
            elif film['profile'] == PROFILES[0]:
                size = (1920, 1080)
            elif film['profile'] == PROFILES[1]:            
                size = (1280, 720)
            elif film['profile'] == PROFILES[2] or film['profile'] == PROFILES[3]: 
                size = (1024, 576)

            sys.stderr.write("WARN: Uncommon Display Aspect Ratio detected\n")
            sys.stderr.write("WARN: Result file may have wrong Display Aspect Ratio\n")
        if size: print "ENC: Resolution changed to: %i x %i" % (size[0], size[1])
        
        self.path = ".avs"
        # QuickTime 
        if format == "mov":
            print "ENC: Clip will be encoded through QuickTime plugin"
            script = "LoadPlugin(\"avsplugins/QTSource.dll\")\n" 
            script += "QTInput(\"" + path + "\")\n"            
        # elif film['codec'] == "dvsd" or film['codec'] == "mpeg-2v":
        else:
            print "ENC: Clip will be encoded through FFmpegSource2 plugin"
            script = "LoadPlugin(\"avsplugins/ffms2.dll\")\n" 
            script += "FFmpegSource2(\"" + path + "\", atrack = -1)\n"
        # else:
        #     print "ENC: Clip will be encoded through DirectShowSource" 
        #     script = "DirectShowSource(\"" + path + "\")\n" 
        
        # Interlacing
        if film['deinterlace'] == DEINTERLACE[0] or film['deinterlace'] == DEINTERLACE[1]:
            intMode = -1 #-1-auto;0-BFF;1-TFF
            perform = 1  #0-no;1-yes
            if film['interlacement'] == "interlaced": intMode = -1
            elif film['interlacement'] == "bff": intMode = 0
            elif film['interlacement'] == "tff": intMode = 1
            
            if film['deinterlace'] == DEINTERLACE[0] and (film['interlacement'] == "ppf" or film['interlacement'] == ""):
                print "ENC: Field order: progressive"
                perform = 0
            
            if (intMode > -1): print "ENC: Found exact field order"
            if (perform == 1):
                print "ENC: Deinterlace method is on (yadif)"
                script += "LoadCPlugin(\"avsplugins/yadif.dll\")\n"
                script += "Yadif(mode=0, order=-1)"
              
        if size: script += "BicubicResize(" + str(size[0]) + ", " + str(size[1]) + ", 0, 0.65)\n"
        script += "ConvertToYV12()\n"
                
        ##saving script        
        self.f = open(self.path, 'w')        
        try:
            self.f.write(script)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.f.close()         
        
    def autodetectDAR(self, film): 
        import MediaInfoDLL
        self.mi = MediaInfoDLL.MediaInfo()
        self.mi.Open(film['path'])
        aspectr = self.mi.Get("Stream_Video", 0, u"DisplayAspectRatio_Original/String")        
        if aspectr:
            print "ENC: Detected %s Display Aspect Ratio" % (aspectr)
            film['aspectr'] = aspectr
        else:
            aspectr = self.mi.Get("Stream_Video", 0, u"DisplayAspectRatio/String")
            if aspectr:
                print "ENC: Detected %s Display Aspect Ratio" % (aspectr)
                film['aspectr'] = aspectr
            else:                
                sys.stderr.write("WARN: Couldn't establish Display Aspect Ratio\n") 
                sys.stderr.write("WARN: Default chosen: %s\n" % ASPECTR[1])
        self.mi.Close()

    def autodetectCODEC(self, film):
        import MediaInfoDLL
        self.mi = MediaInfoDLL.MediaInfo()
        self.mi.Open(film['path'])
        film['codec'] = self.mi.Get("Stream_Video", 0, u"Codec").lower()
        print "ENC: Detected \"%s\" codec" % film['codec']
        self.mi.Close()

    def autodetectINTERLACEMENT(self, film):
        import MediaInfoDLL
        self.mi = MediaInfoDLL.MediaInfo()
        self.mi.Open(film['path'])
        film['interlacement'] = self.mi.Get("Stream_Video", 0, u"Interlacement").lower()
        print "ENC: Detected \"%s\" field order" % film['interlacement']
        self.mi.Close()
        
class BATENCV:
    def __init__(self, parent, film):        
        self.avs = AVS(film)        
        self.cmd = None    
        
        self.generateCMD(film)
        import codecs
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
            parent.encstatus = -6
            parent.clear()
        self.bat.close()           
    
    def generateCMD(self, film):
        ## HD 1080p    
        if film['profile'] == PROFILES[0]:
            vb = 3472            
            rt = 40
            me = 'umh'
            merange = 32
        ## HD 720p
        if film['profile'] == PROFILES[1]:
            vb = 2272           
            rt = 35
            me = 'umh'
            merange = 32
        ## SD 576p            
        if film['profile'] == PROFILES[2]:
            vb = 1572            
            rt = 30
            me = 'umh'
            merange = 32
        ## YT 576p
        if film['profile'] == PROFILES[3]:
            vb = 1572            
            rt = 30
            me = 'hex'
            merange = 16
        ## MQ 336p
        if film['profile'] == PROFILES[4]:
            vb = 552            
            rt = 20
            me = 'umh'
            merange = 16
        ## PREV
        if film['profile'] == PROFILES[5]:
            vb = 2300#652
            rt = 50
        ## SHIT
        if film['profile'] == PROFILES[6]:
            vb = 25000
            rt = 50
        
        if film['profile'] == PROFILES[0] or \
            film['profile'] == PROFILES[1] or \
            film['profile'] == PROFILES[2] or \
            film['profile'] == PROFILES[4]:    
            self.cmd = \
            X264 + " --pass 1 --bitrate %i --ratetol %i --stats \"output\\.stats\" --ref 16 --open-gop normal --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all " % (vb, rt) + \
            "--me %s --merange %i --output NUL %s" % (me, merange, self.avs.path) + \
            "\n" + X264 + " --pass 2 --bitrate %i --ratetol %i --stats \"output\\.stats\" --ref 16 --open-gop normal --bframes 16 --b-adapt 2 --no-fast-pskip --direct auto --deblock -1:-1 --subme 10 --partitions all " % (vb, rt) + \
            "--me %s --merange %i --output \"output\\%s_video.mp4\" %s" % (me, merange, film['name'], self.avs.path)
        elif film['profile'] == PROFILES[5] or film['profile'] == PROFILES[6]:
            self.cmd = \
            X264 + " --bitrate %i --ratetol %i --preset fast --output \"output\\%s_video.mp4\" %s" % (vb, rt, film['name'], self.avs.path)        
        elif film['profile'] == PROFILES[3]:
            self.cmd = \
            X264 + " --bitrate %i --ratetol %i --ref 3 --bframes 16 --deblock -1:-1 --subme 7 --partitions all --me %s --merange %i " % (vb, rt, me, merange) + \
            " --output \"output\\%s_video.mp4\" %s" % (film['name'], self.avs.path)            
        else:
            sys.stderr.write("ERROR: Wrong profile ID!/n")
        
            
class BATENCA:
    def __init__(self, film):        
        self.avs = AVS(film)        
        self.cmd = None    
        
        self.generateCMD(film)
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.bat.close()           
    
    def generateCMD(self, film):
        if film['profile'] == PROFILES[0]:            
            ab = 128000            
        if film['profile'] == PROFILES[1]:           
            ab = 128000  
        if film['profile'] == PROFILES[2]:           
            ab = 128000  
        if film['profile'] == PROFILES[3]:           
            ab = 128000  
        if film['profile'] == PROFILES[4]:           
            ab = 128000  
        if film['profile'] == PROFILES[5]:            
            ab = 256000#48000
        if film['profile'] == PROFILES[6]:            
            ab = 320000   

        ##format searcher
        self.filter = re.compile(r"(.+?)(\.([^\.]*))?$")
        format = self.filter.search(film['path']).group(3).lower()
                
        if film['codec'] == "mpeg-2v":                        
            self.cmd = \
            BEPIPE + " --script \"FFAudioSource(^%s^)\" | " % film['path'] + NERO + " -br %i -if - -of \"output\\%s_audio.mp4\"" % (ab, film['name'])
        else:
            self.cmd = \
            BEPIPE + " --script \"DirectShowSource(^%s^)\" | " % film['path'] + NERO + " -br %i -if - -of \"output\\%s_audio.mp4\"" % (ab, film['name'])          

class BATENCM:
    def __init__(self, film):
        self.cmd = None    
        
        self.generateCMD(film)
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.bat.close()           
    
    def generateCMD(self, film):        
        self.cmd = \
        MP4BOX + " -new -add \"output\\%s_video.mp4\" -add \"output\\%s_audio.mp4\" \"output\\%s.mp4\"" % (film['name'], film['name'], film['name'])
        
        ## YT
        if film['profile'] == PROFILES[3]: 
            self.cmd = \
            MP4BOX + " -new -add \"output\\%s_video.mp4\" -add \"output\\%s_audio.mp4\" \"output\\%s.mp4\"" % (film['name'], film['name'], film['name'])
            self.cmd += "\n" + MP4BOX + " -split 600 \"output\\%s.mp4\"" % film['name']
        
        
class BATTAG:
    def __init__(self, film, md5 = None):      
        self.cmd = None 
        self.md5value = md5   
                
        self.generateCMD(film)
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.bat.close()
      
    def generateCMD(self, film):
        import Personal        
        year = str(time.localtime()[0])
        month = str(time.localtime()[1]).zfill(2)
        day = str(time.localtime()[2]).zfill(2)
        self.cmd = AP + " output/%s.mp4 --overWrit -D %s-%s-%s" % (film['name'], year, month, day)
        if EDITION == 0: self.cmd += "\n" + AP + " output/%s.mp4 --overWrit --copyright \"Studencka Telewizja Internetowa TVPW\"" % film['name']
        if EDITION == 1: self.cmd += "\n" + AP + " output/%s.mp4 --overWrit --copyright \"%s\"" % (film['name'], Personal.loadPersonalData())
        if film['thumb'] != THUMB[0]:            
            path = re.sub(r'\\', r'/', film['thumb'])            
            self.cmd += "\n" + AP + " output/%s.mp4 --overWrit --artwork \"%s\"" % (film['name'], path)
        else:
            sys.stderr.write("ENC: No thumbnail path found - please add thumbnail later\n")
        if film['apl'] == APL[1]:
            self.cmd += "\n" +AP + " output/%s.mp4 --overWrit --advisory %s" % (film['name'],APL[1])
        else:
            self.cmd += "\n" +AP + " output/%s.mp4 --overWrit --advisory %s" % (film['name'],APL[0])
        self.cmd += "\n" +AP + " output/%s.mp4 --overWrit --encodingTool \"%s;%s;%s;%s\"" % (film['name'], ZETid, ZETversion, film['profile'], film['aspectr'])   
        self.cmd += "\n" +AP + " output/%s.mp4 --overWrit --keyword %s" % (film['name'], self.md5value)
    
  
   
