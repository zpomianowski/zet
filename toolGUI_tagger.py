__id__ = "ZbychuTagger"
__version__ = "0.1.2 BETA"
__reldate__ = "30.08.2009"
import wx
import wx.calendar
import subprocess
import os
import sys
from constants import *

class Tagger(wx.Frame):
    def __init__(self, parent, id):
        title = __id__ + " v" + __version__ + " Release: " + __reldate__
        wx.Frame.__init__(self, parent, id, title, size=(400, 600))
        self.SetIcon(wx.Icon("data/tvpw.ico", wx.BITMAP_TYPE_ICO))
        
        self.fpicker = wx.FilePickerCtrl(self, -1, path="Click here and choose mp4 file",message="Choose mp4 file", wildcard="Main Concept AVC (*.mp4)|*.mp4|")
        ## date
        self.date = wx.StaticText(self, -1, "Date:")
        self.datecal = wx.calendar.CalendarCtrl(self, -1)
        ## copyright
        self.cpyr = wx.StaticText(self, -1, "Copyright:")
        self.cpyrtext = wx.TextCtrl(self, -1, )
        ## md5
        self.md5 = wx.StaticText(self, -1, "MD5 checksum:")
        self.md5text = wx.TextCtrl(self, -1, )
        ## thumb
        self.thumb = wx.StaticText(self, -1, "Thumbnail:")
        self.thumbpicker = wx.FilePickerCtrl(self, 3, message="Choose thumbnail", wildcard=GFORMATS)
        self.bitmap = wx.StaticBitmap(self)
        self.defaultthumb = wx.Bitmap("data/nothumb.png")
        self.bitmap.SetBitmap(self.defaultthumb)
        ## explicit content
        self.APLcheckbox = wx.CheckBox(self, -1, "Explicit content:", style=wx.ALIGN_RIGHT)
        ## encoding tool
        self.etoolenct = wx.StaticText(self, -1, "Encoding tool:")
        self.etoolenc = wx.TextCtrl(self, -1, size=(140,-1))
        self.etoolvert = wx.StaticText(self, -1, "Version:")
        self.etoolver = wx.TextCtrl(self, -1, size=(140,-1))
        self.etoolprot = wx.StaticText(self, -1, "Profile:")
        self.etoolpro = wx.Choice(self, -1, size=(140,-1), choices=PROFILES)
        self.etoolaspt = wx.StaticText(self, -1, "Aspect ratio:")
        self.etoolasp = wx.Choice(self, -1, size=(50,-1), choices=ASPECTR) 
        
        self.read = wx.Button(self, 1, "Read")
        self.write = wx.Button(self, 2, "Write")      
        
        self.out = None
        self.err = None
        
        self.__setProperties()
        self.__doLayout()
        
    def __setProperties(self):
        self.enable(False)
        self.Bind(wx.EVT_BUTTON, self.OnRead, id=1)
        self.Bind(wx.EVT_BUTTON, self.OnWrite, id=2)
        self.Bind(wx.EVT_FILEPICKER_CHANGED, self.OnThumb, id=3)
        
    def __doLayout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)        
        sizer.Add(self.fpicker, 0, wx.EXPAND, 10)
        sizer.Add(wx.StaticLine(self, -1, size=(-1,14)), 0, wx.EXPAND, 10)
        sizer.Add(self.date, 0, wx.EXPAND, 10)
        sizer.Add(self.datecal, 1, wx.EXPAND, 10)
        sizer.Add(wx.StaticLine(self, -1, size=(-1,14)), 0, wx.EXPAND, 10)
        sizer.Add(self.cpyr, 0, wx.EXPAND, 10)
        sizer.Add(self.cpyrtext, 0, wx.EXPAND, 10)
        sizer.Add(self.md5, 0, wx.EXPAND, 10)
        sizer.Add(self.md5text, 0, wx.EXPAND, 10)
        sizer.Add(wx.StaticLine(self, -1, size=(-1,14)), 0, wx.EXPAND, 10)
        sizer.Add(self.thumb, 0, wx.EXPAND, 10)
        sizer.Add(self.thumbpicker, 0, wx.EXPAND, 10)
        sizer.Add(self.bitmap, 0, wx.ALIGN_CENTER, 0)
        sizer.Add(wx.StaticLine(self, -1, size=(-1,14)), 0, wx.EXPAND, 10)
        sizer.Add(self.APLcheckbox, 0, wx.ALIGN_LEFT, 10)
        sizer.Add(wx.StaticLine(self, -1, size=(-1,14)), 0, wx.EXPAND, 10)
        sizer.Add(self.etoolenct, 0, wx.EXPAND, 10)
        sizer.Add(self.etoolenc, 0, 0, 10)
        sizer.Add(self.etoolvert, 0, wx.EXPAND, 10)
        sizer.Add(self.etoolver, 0, 0, 10)
        sizer.Add(self.etoolprot, 0, wx.EXPAND, 10)
        sizer.Add(self.etoolpro, 0, 0, 10)
        sizer.Add(self.etoolaspt, 0, wx.EXPAND, 10)
        sizer.Add(self.etoolasp, 0, 0, 10)
        sizer.Add(wx.StaticLine(self, -1, size=(-1,14)), 0, wx.EXPAND, 10)
        bsizer = wx.GridSizer(1, 2, 0, 0)
        bsizer.Add(self.read, 0, wx.EXPAND)
        bsizer.Add(self.write, 0, wx.EXPAND)
        sizer.Add(bsizer, 0, wx.EXPAND, 10)
        self.SetSizer(sizer)
        self.Layout()
        
    def OnRead(self, event):
        print __id__ + ": readind data..."

        path = os.path.normpath(self.fpicker.GetPath())

        if os.path.exists(path) == False:
            sys.stderr.write(__id__ + ": Wrong path...\n")
            return

        cmd = AP + " \"" + path + "\" -t"
        out,err = self.run(cmd)
        #print out
        import re
        
        ## APL
        pAPL = re.compile(r'rtng\"\scontains:\s(.*)') 
        tup = pAPL.search(out)
        if tup:
            APLv = tup.group(1).strip()
            if APLv == "Explicit Content":
                self.APLcheckbox.SetValue(True)
            else:
                self.APLcheckbox.SetValue(False)
        else:
            self.APLcheckbox.SetValue(False)
            print __id__ + ": no APL info found"
        
        ## md5
        pmd5 = re.compile(r'\"keyw\"\scontains:\s(.*)')
        tup = pmd5.search(out)
        if tup:
            md5v = tup.group(1).strip()
            self.md5text.SetValue(md5v.upper())
        else:
            self.md5text.SetValue("None")
            print __id__ + ": no MD5 checksum info found"
            
        ## copyright
        pcpyr = re.compile(r'\"cprt\"\scontains:\s(.*)')
        tup = pcpyr.search(out)
        if tup:
            cpyrv = tup.group(1).strip()
            self.cpyrtext.SetValue(cpyrv)
        else:
            self.cpyrtext.SetValue("None")
            print __id__ + ": no copyright info found"
            
        ## encodingtool
        ptoo = re.compile(r'too\"\scontains:\s(.*?);(.*?);(.*?);(.*)')
        tup = ptoo.search(out)
        if tup:
            self.etoolenc.SetValue(tup.group(1).strip())
            self.etoolver.SetValue(tup.group(2).strip())
            for i in xrange(len(PROFILES)):
                if tup.group(3).strip() == PROFILES[i]:
                    self.etoolpro.SetStringSelection(PROFILES[i])
            for i in xrange(len(ASPECTR)):
                if tup.group(4).strip() == ASPECTR[i]:
                    self.etoolasp.SetStringSelection(ASPECTR[i])            
        else:
            self.etoolenc.SetValue("None")
            self.etoolver.SetValue("None")
            self.etoolpro.Select(-1)
            self.etoolasp.Select(-1)
            print __id__ + ": no encodingtool info found"
        ## date
        pdate = re.compile(r'purd\"\scontains:\s(.*?)-(.*?)-(.*)')
        tup = pdate.search(out)
        if tup:
            datetime = wx.DateTime()
            datetime.Set(int(tup.group(3)), int(tup.group(2))-1, int(tup.group(1)))
            self.datecal.SetDate(datetime)
        else:
            datetime = wx.DateTime()
            datetime.Set(1, 0, 0)
            self.datecal.SetDate(datetime)
            print __id__ + ": no date info found"
            
        ## thumb
        cmd = AP + " \"" + path + "\" -E"
        out,err = self.run(cmd)
        part = re.compile(r'to file:\s(.*?)\n')
        tup = part.search(out)
        if tup:            
            thumbpath = os.path.normpath(tup.group(1)[:-1])
            self.thumbpicker.SetPath(thumbpath)
            self.bitmap.SetBitmap(wx.Bitmap(thumbpath)) 
        else:
            self.thumbpicker.SetPath("None")
            self.bitmap.SetBitmap(self.defaultthumb)
            print __id__ + ": no artwork found" 
        
        
        self.Layout()
        self.Refresh()
        dial = wx.MessageDialog(self, 'Tags imported succesfully!', 'Tags imported', 
            wx.OK |  wx.ICON_INFORMATION) 
        dial.ShowModal()
        self.enable(True)
        
    def OnWrite(self, event):
        dial = wx.MessageDialog(self, 'Are You sure?', 'Writing tags', 
            wx.YES_NO | wx.YES_DEFAULT | wx.ICON_EXCLAMATION) 
        if dial.ShowModal() == wx.ID_NO:
            return
        self.enable(False)
        path = os.path.normpath(self.fpicker.GetPath())
        date = self.datecal.GetDate()
        cmd = \
            AP + " \"" + path + "\" --artwork REMOVE_ALL" + \
            "\n" + AP + " \"" + path + "\" --overWrit -D %s-%s-%s" % (date.GetYear(), str(date.GetMonth()+1).zfill(2), str(date.GetDay()).zfill(2)) + \
            "\n" + AP + " \"" + path + "\" --overWrit --copyright \"%s\"" % self.cpyrtext.GetValue() + \
            "\n" + AP + " \"" + path + "\" --overWrit --keyword \"%s\"" % self.md5text.GetValue().lower()
        
        if self.APLcheckbox.GetValue() == True:
            cmd += "\n" + AP + " \"" + path + "\" --overWrit --advisory %s" % APL[1]
        else:
            cmd += "\n" + AP + " \"" + path + "\" --overWrit --advisory %s" % APL[0]
         
        id = self.etoolenc.GetValue()
        ver = self.etoolver.GetValue()
        pro = self.etoolpro.GetStringSelection()
        asp = self.etoolasp.GetStringSelection()
        print asp
        cmd += "\n" + AP + " \"" + path + "\" --overWrit --encodingTool \"%s;%s;%s;%s\"" % (id, ver, pro, asp)   
        
        if os.path.exists(self.thumbpicker.GetPath()):        
            cmd += "\n" + AP + " \"" + path + "\" --overWrit --artwork \"%s\"" % (self.thumbpicker.GetPath())
        else:
            sys.stderr.write(__id__ + ": No thumbnail path found - please add thumbnail later\n")
 
        out,err = self.run(cmd)
        self.enable(True)
        dial = wx.MessageDialog(self, 'Writing tags finished', 'Tags exported', 
            wx.OK |  wx.ICON_INFORMATION) 
        dial.ShowModal()
        if err != "": sys.stderr.write(__id__ + ": " + err)
    
    def OnThumb(self, event):
        pic = wx.Bitmap(self.thumbpicker.GetPath())
        size = pic.GetSize()
        # if size[0] != 100 or size[1] != 75:
        #     sys.stderr.write(__id__ + ": invalid thumbnail size! It has to be 100x75!\n")
        #     dial = wx.MessageDialog(self, 'Wrong size of thumbnail. It has to be 100x75!', 'Error', 
        #         wx.OK |  wx.ICON_EXCLAMATION)
        #     dial.ShowModal()
        #     self.thumbpicker.SetPath("")
        #     self.bitmap.SetBitmap(self.defaultthumb)
        #     return        
        self.bitmap.SetBitmap(pic)
        self.Layout()
        self.Refresh()
        
    def enable(self, flag):
        if flag == True:
            self.datecal.Enable(True)
            self.cpyrtext.Enable(True)
            self.md5text.Enable(True)
            self.thumbpicker.Enable(True)
            self.APLcheckbox.Enable(True)
            self.etoolenct.Enable(True)
            self.etoolenc.Enable(True)
            self.etoolver.Enable(True)
            self.etoolpro.Enable(True)
            self.etoolasp.Enable(True)
        else:
            self.datecal.Enable(False)
            self.cpyrtext.Enable(False)
            self.md5text.Enable(False)
            self.thumbpicker.Enable(False)
            self.APLcheckbox.Enable(False)
            self.etoolenct.Enable(False)
            self.etoolenc.Enable(False)
            self.etoolver.Enable(False)
            self.etoolpro.Enable(False)
            self.etoolasp.Enable(False)
            
    def run(self, cmd):
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        hP = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startupinfo)
        (out,err) = hP.communicate()
        return out,err
         
    def report(self, message, infotype):
        pass
        
class MyApp(wx.App):
    def OnInit(self):
        frame = Tagger(None, -1)
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()