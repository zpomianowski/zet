import sys
import os
import time
import wx
import ConfigParser

from constants import *
from collections import deque
from ftplib import FTP
from PEcrypt import PEcrypt
import base64

class FTPSender:
    def __init__(self, ctrlframe, grid):
        self.ctrlframe = ctrlframe
        self.grid = grid
        self.queue = deque()
        
        self.pe = PEcrypt("chujCIwCYCE")
        
        self.user = None
        self.passw = None
        self.server = None
        self.port = None
 
        self.filesize = None
        self.total = 0
        
        self.runflag = False
        
        self.loadSettings()
    
    def addTask(self, index, path):
        print "FTP: [%d, %s] added to send list" % (index, path)
        self.queue.appendleft([index, path])
        
    def startSending(self, callback=None):
        if self.runflag == True:
            return
        
        while len(self.queue) != 0:
            self.runflag = True
            current = self.queue.pop()
            
            try:
                ## connect
                ftp = FTP()
                ftp.connect(self.server, self.port) 
                ftp.login(self.user, self.passw)
            except Exception:
                sys.stderr.write("FTP: Couldn't connect/login!\n")
                self.grid.SetCellValue(current[0], C_STATE, STATES[5])
                return
            ## send
            try:
                filename = os.path.basename(current[1])
                self.filesize = os.stat(current[1]).st_size
            except Exception:
                sys.stderr.write("FTP: Couldn't find file!\n")
                self.grid.SetCellValue(current[0], C_STATE, STATES[5])
                return
            try:
                if EDITION == 1: ftp.cwd('pub')
                ftp.storbinary('STOR ' + filename, open(current[1], 'rb'), callback=self.updateGaugeFTP)
            except Exception:
                sys.stderr.write("FTP: Requested action not taken. Couldn't send file!\n")
                sys.stderr.write("FTP: Permission denied or filename already exists!\n")
                self.grid.SetCellValue(current[0], C_STATE, STATES[5])
                return
            
            ## successfully finished
            ftp.close()
            print "FTP: File " + filename + " successfully sent"
            if EDITION == 0: print "FTP: Direct LINK: http://tvpw.pl/oceny/" + filename
            self.grid.SetCellValue(current[0], C_STATE, STATES[4])
            time.sleep(5)
            self.ctrlframe.gauge_ftp.SetValue(0)
            self.total = 0            

        self.runflag = False
        
    def updateGaugeFTP(self, buffer):
        import math
        self.total += len(buffer)-1        
        status = math.ceil(float(self.total)/self.filesize*100)
        self.ctrlframe.gauge_ftp.SetValue(status)

    def loadSettings(self):
        config = ConfigParser.RawConfigParser()
        config.read(cfgfile)
        
        try:
            ## load settings
            self.user = self.pe.Crypt(base64.b64decode(config.get('ftp', 'user')))
            self.passw = self.pe.Crypt(base64.b64decode(config.get('ftp', 'pass')))
            self.server = self.pe.Crypt(base64.b64decode(config.get('ftp', 'serv')))
            try:
                self.port = int(self.pe.Crypt(base64.b64decode(config.get('ftp', 'port'))))
            except Exception:
                pass
        except Exception:
            print "FTP: Define new settings..."
            ftpdiag = FTPSetDiag(None, -1, 'First run...')
            if ftpdiag.ShowModal() == 101:
                self.user = ftpdiag.user.GetValue()
                self.passw = ftpdiag.passw.GetValue()
                self.server = ftpdiag.server.GetValue()
                self.port = ftpdiag.port.GetValue()
               
                config = ConfigParser.RawConfigParser()
                config.read(cfgfile)
                try:
                    config.add_section('ftp')
                except Exception:
                    pass
                
                config.set('ftp', 'user', base64.b64encode(self.pe.Crypt(self.user)))
                config.set('ftp', 'pass', base64.b64encode(self.pe.Crypt(self.passw)))
                config.set('ftp', 'serv', base64.b64encode(self.pe.Crypt(self.server)))
                config.set('ftp', 'port', base64.b64encode(self.pe.Crypt(self.port)))
                
                with open(cfgfile, 'wb') as configfile:
                    config.write(configfile)
                    
                ## load new settings
                self.user = self.pe.Crypt(base64.b64decode(config.get('ftp', 'user')))
                self.passw = self.pe.Crypt(base64.b64decode(config.get('ftp', 'pass')))
                self.server = self.pe.Crypt(base64.b64decode(config.get('ftp', 'serv')))
                try:
                    self.port = int(self.pe.Crypt(base64.b64decode(config.get('ftp', 'port'))))
                except Exception:
                    pass
                
                ## test ftp
                try:
                    ## connect
                    ftp = FTP()
                    print "FTP: " + ftp.connect(self.server, self.port) 
                    print "FTP: " + ftp.login(self.user, self.passw)
                    d = wx.MessageDialog(None, "Result: OK.",
                                        "FTP connection test routine", wx.OK | wx.ICON_INFORMATION)
                    d.ShowModal()
                except Exception:
                    d = wx.MessageDialog(None, "Result: FAILED\nPlease, ensure the settings of ftp connection are correct.",
                                        "FTP connection test routine", wx.OK | wx.ICON_ERROR)
                    d.ShowModal()
                ftp.close()
            ftpdiag.Destroy()
    
    def defineSettings(self):
        config = ConfigParser.RawConfigParser()
        config.read(cfgfile)
        
        ftpdiag = FTPSetDiag(None, -1, 'Define FTP connection settings')
        
        try:
            ## load settings
            self.user = self.pe.Crypt(base64.b64decode(config.get('ftp', 'user')))
            self.passw = self.pe.Crypt(base64.b64decode(config.get('ftp', 'pass')))
            self.server = self.pe.Crypt(base64.b64decode(config.get('ftp', 'serv')))
            ftpdiag.user.SetValue(self.user)
            ftpdiag.passw.SetValue(self.passw)
            ftpdiag.server.SetValue(self.server)            
            try:
                self.port = int(self.pe.Crypt(base64.b64decode(config.get('ftp', 'port'))))
                ftpdiag.port.SetValue(str(self.port))
            except Exception:
                sys.stderr.write("FTP: Error - port must be a number!\n")
        except Exception:
            sys.stderr.write("FTP: Error - couldn't load settings!\n")
        
        if ftpdiag.ShowModal() == 101:
            self.user = ftpdiag.user.GetValue()
            self.passw = ftpdiag.passw.GetValue()
            self.server = ftpdiag.server.GetValue()
            self.port = ftpdiag.port.GetValue()
           
            config = ConfigParser.RawConfigParser()
            config.read(cfgfile)
            try:
                config.add_section('ftp')
            except Exception:
                pass
            
            config.set('ftp', 'user', base64.b64encode(self.pe.Crypt(self.user)))
            config.set('ftp', 'pass', base64.b64encode(self.pe.Crypt(self.passw)))
            config.set('ftp', 'serv', base64.b64encode(self.pe.Crypt(self.server)))
            config.set('ftp', 'port', base64.b64encode(self.pe.Crypt(self.port)))
            
            with open(cfgfile, 'wb') as configfile:
                config.write(configfile)
                
            ## load new settings
            self.user = self.pe.Crypt(base64.b64decode(config.get('ftp', 'user')))
            self.passw = self.pe.Crypt(base64.b64decode(config.get('ftp', 'pass')))
            self.server = self.pe.Crypt(base64.b64decode(config.get('ftp', 'serv')))
            try:
                self.port = int(self.pe.Crypt(base64.b64decode(config.get('ftp', 'port'))))
            except Exception:
                pass
            
            ## test ftp
            try:
                ## connect
                ftp = FTP()
                print "FTP: " + ftp.connect(self.server, self.port) 
                print "FTP: " + ftp.login(self.user, self.passw)
                d = wx.MessageDialog(None, "Result: OK.",
                                    "FTP connection test routine", wx.OK | wx.ICON_INFORMATION)
                d.ShowModal()
            except Exception:
                print Exception
                d = wx.MessageDialog(None, "Result: FAILED\nPlease, ensure the settings of ftp connection are correct.",
                                    "FTP connection test routine", wx.OK | wx.ICON_ERROR)
                d.ShowModal()
            ftp.close()
        ftpdiag.Destroy()

class FTPSetDiag(wx.Dialog):
    def __init__(self, parent, id, title):
        wx.Dialog.__init__(self, parent, id, title, size=(300, 230))

        panel = wx.Panel(self, -1)
        
        wx.StaticBox(panel, -1, 'Ftp settings', (5, 5), (285, 150))
        wx.StaticText(panel, -1, 'User:', (15, 30), style=wx.RB_GROUP)
        self.user = wx.TextCtrl(panel, -1, '', (80, 28), (200, 18))
        wx.StaticText(panel, -1, 'Password:', (15, 60), style=wx.RB_GROUP)
        self.passw = wx.TextCtrl(panel, -1, '', (80, 58), (200, 18), style=wx.TE_PASSWORD)
        wx.StaticText(panel, -1, 'Server:', (15, 90), style=wx.RB_GROUP)
        self.server = wx.TextCtrl(panel, -1, 'ftp.tvpw.pl', (80, 88), (200, 18))
        wx.StaticText(panel, -1, 'Port:', (15, 120), style=wx.RB_GROUP)
        self.port = wx.TextCtrl(panel, -1, '', (80, 118), (40, 18))
        
        okeyButton = wx.Button(self, 101, 'Ok', size=(70, 30))
        cancButton = wx.Button(self, 102, 'Cancel', size=(70, 30))
        self.SetAffirmativeId(101)
        self.SetEscapeId(102)
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(okeyButton, 1)
        hbox.Add(cancButton, 1, wx.LEFT, 5)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(panel)
        vbox.Add(hbox, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 10)

        self.SetSizer(vbox)
                
if __name__ == '__main__':
    ftp = FTPSender(None, None)
    ftp.addTask(0, "output\PREVIEW_sample.mp4")
    ftp.startSending()
    FTPprogress.close()

        