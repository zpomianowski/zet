import wx
import os
import sys
import threading
import platform

from propSplitter import PropSplitter
from tabGUI import *
from FTPSender import *
from constants import *

class cPanel(wx.Panel):
    def __init__(self, parent, grid, id, **kwargs):
        wx.Panel.__init__(self, parent, id, **kwargs)
        self.parent = parent
        self.grid = grid       
        self.control = wx.ToggleButton(self, 1, 'Start/Stop', (60, 20))
        ## progress bars
        color_task = wx.Color(0,0,150)
        self.gauge_task = wx.Gauge(self, -1, 100, size=(250, 7))        
        self.gauge_task.SetForegroundColour(color_task)
        
        color_total = wx.Color(0,0,100)
        self.gauge_total = wx.Gauge(self, -1, 100, size=(250, 7))
        self.gauge_task.SetForegroundColour(color_total)
        
        color_ftp = wx.Color(0,150,0)
        self.gauge_ftp = wx.Gauge(self, -1, 100, size=(250, 7))
        self.gauge_ftp.SetForegroundColour(color_ftp)
        
        self.progress = 0
        ## textfield
        self.console = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.TE_RICH | wx.HSCROLL)
        self.redirstdo = RedirStdOut(self.console)
        self.redirstde = RedirStdErr(self.console)
        self.redirstdi = RedirStdInp(self.console)
        ## ENCODING control
        self.encoder = None
        self.thread = None       
        self.flag = False
        ## FTPSENDer
        self.ftpsender = FTPSender(self, self.grid)
                        
        self.__setProperties()
        self.__doLayout()    
        
    def __setProperties(self):
        sys.stdout = self.redirstdo        
        sys.stderr = self.redirstde
        sys.stdin = self.redirstdi
        
        self.gauge_task.SetValue(0)
        self.gauge_total.SetValue(0)
        self.gauge_ftp.SetValue(0)
        
        self.console.SetMinSize((400, 64))
        self.console.SetMaxLength(8000)
        
        self.Bind(wx.EVT_TOGGLEBUTTON, self.__OncontrolChanged, id=1)
                       
    def __doLayout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.console, 1, wx.EXPAND, 0)
        sizer.Add(self.control, 0, wx.EXPAND, 0)
        sizer.Add(self.gauge_task, 0, wx.EXPAND, 0)
        sizer.Add(self.gauge_total, 0, wx.EXPAND, 0) 
        sizer.Add(self.gauge_ftp, 0, wx.EXPAND, 0)        
        self.SetSizer(sizer)
        self.Layout()
        
    def __OncontrolChanged(self, event):        
        value = self.control.GetValue()
        if value == True:
            self.flag = True            
            self.thread = threading.Thread(target=self.enc)
            self.thread.start()            
        if value == False: 
            self.flag = False           
            if self.encoder:
                t = threading.Thread(target=self.stop)
                t.start()
       
    def enc(self):
        list = []
        self.progress = 0
        ## gather list of items to encode and set gauge_total
        for x in range(self.grid.GetNumberRows()):
            if self.grid.GetCellValue(x, C_STATE) == STATES[0] or self.grid.GetCellValue(x, C_STATE) == STATES[8]:
                if self.grid.GetCellValue(x, C_STATE) == STATES[8]: 
                    self.grid.SetCellValue(x, C_STATE, STATES[1])
                    self.grid.SetCellBackgroundColour(x, C_STATE, wx.Colour(0, 0, 150))
                if EDITION == 0:
                    list.append(x) 
                    self.grid.SetCellValue(x, C_STATE, STATES[1])
                if EDITION == 1:
                    if self.grid.GetCellValue(x, C_THUMB) == THUMB[0]:
                        sys.stderr.write("WARN: No thumbnail found, task \"%s\" not enqueued!\n" % self.grid.GetCellValue(x, C_PATH))
                    else:
                        list.append(x) 
                        self.grid.SetCellValue(x, C_STATE, STATES[1])
            else:
                self.progress += 1
        if self.grid.GetNumberRows() > 0:
            self.gauge_total.SetValue(float(self.progress)/self.grid.GetNumberRows()*100)
        ## no items
        if len(list) == 0:
            self.control.SetValue(False)
            print "INFO: No items to encode"
            self.flag = False
            return
        ## start ENCODING for each item
        self.grid.EnableEditing(False)
        for x in list:
            encoder = self.grid.GetCellValue(x, C_ENC)
            format = None
            
            ## Windows
            if platform.system() == "Windows":
                if encoder == ENCODING[0]:
                    import enc_h264aac as encCore
                    format = ".mp4"
                else:
                    import enc_xvidac3 as encCore
                    format = ".avi"
                
            if self.flag == True:
                print "INFO: Encoding: " + str(x+1) + "/" + str(len(list)) + " started..."
                film = {
                    'name': self.grid.GetCellValue(x, C_NAME),
                    'path': self.grid.GetCellValue(x, C_PATH),
                    'thumb': self.grid.GetCellValue(x, C_THUMB),
                    'apl': self.grid.GetCellValue(x, C_APL),
                    'aspectr': self.grid.GetCellValue(x, C_ASPECT),
                    'profile': self.grid.GetCellValue(x, C_PROFILE),
                    'deinterlace': self.grid.GetCellValue(x, C_DEINTERLACE), 
                    'ftp': self.grid.GetCellValue(x, C_FTP)
                }
                
                self.grid.SetCellValue(x, C_STATE, STATES[2])
                self.encoder = encCore.Encoder(self, x, film)                
                ret = self.encoder.start()
                
                if ret < 0:
                    self.grid.SetCellValue(x, C_STATE, STATES[7])
                    self.grid.SetCellBackgroundColour(x, C_STATE, wx.Colour(255, 0, 0))
                    if ret == -6:
                        self.grid.SetCellValue(x, C_STATE, STATES[6])
                        self.grid.SetCellBackgroundColour(x, C_STATE, wx.Colour(255, 165, 0))
                    continue
                if ret == 1:
                    self.grid.SetCellValue(x, C_STATE, STATES[8])
                    self.grid.SetCellBackgroundColour(x, C_STATE, wx.Colour(255, 165, 0))
                    continue
                
                
                self.grid.SetCellValue(x, C_STATE, STATES[3])
                self.progress += 1        
                self.gauge_total.SetValue(float(self.progress)/self.grid.GetNumberRows()*100)
                self.encoder.clear()
                
                print "INFO: Encoding process finished"
                
                if film['ftp'] == FTPSEND[1]:
                    print "INFO: Sending to TVPW server..."
                    self.ftpsender.addTask(x, "output\\" + self.grid.GetCellValue(x, C_NAME) + format)
                    print "FTP: Awaiting %d tasks" % (len(self.ftpsender.queue))
                    t = threading.Thread(target=self.ftpsender.startSending)
                    t.start()    
                    #self.ftpsender.startSending()
            else:
                print "INFO: Encoding: " + str(x+1) + "/" + str(len(list)) + " canceled!"
                self.grid.SetCellValue(x, C_STATE, STATES[0]) 
        self.grid.EnableEditing(True)
        self.control.SetValue(False)  
        self.flag = False              

    def stop(self):
        dial = wx.MessageDialog(self, 'Aborted process can\'t be continued.\nCurrent progress will be lost. Proceed?', 'Warning?', 
            wx.YES_NO | wx.NO_DEFAULT | wx.ICON_EXCLAMATION)
        if dial.ShowModal() == wx.ID_YES:        
            self.control.Enable(False)         
            print "INFO: Terminating ENCODING process - please, wait a moment..."
            self.encoder.stop()          
            import time
            time.sleep(1)            
            #self.grid.SetCellValue(self.encoder.id, C_STATE, STATES[0])            
            self.grid.ForceRefresh()
            self.gauge_task.SetValue(0)
            self.gauge_total.SetValue(0)
            self.progress = 0
            self.control.Enable(True)
        else:
            self.control.SetValue(True)
                   
class RedirStdOut(object):
    def __init__(self,aWxTextCtrl):
        self.out=aWxTextCtrl        
        
    def write(self,string):
        try: 
            self.out.SetDefaultStyle(wx.TextAttr(wx.BLUE, None, wx.Font(7, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL ,wx.FONTWEIGHT_NORMAL)))
            self.out.ShowPosition(self.out.GetLastPosition())
            self.out.AppendText(string)          
        except Exception:
            pass
            
class RedirStdErr(object):
    def __init__(self,aWxTextCtrl):
        self.out=aWxTextCtrl
                
    def write(self,string):
        try:       
            self.out.SetDefaultStyle(wx.TextAttr(wx.RED, None, wx.Font(7, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL ,wx.FONTWEIGHT_NORMAL))) 
            self.out.ShowPosition(self.out.GetLastPosition())    
            self.out.AppendText(string)   
        except Exception:
            pass
            
class RedirStdInp(object):
    def __init__(self,aWxTextCtrl):
        self.out=aWxTextCtrl
                
    def write(self,string):       
        self.out.SetDefaultStyle(wx.TextAttr(wx.GREEN, None, wx.Font(7, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL ,wx.FONTWEIGHT_NORMAL))) 
        self.out.ShowPosition(self.out.GetLastPosition())           
        self.out.AppendText(string)
