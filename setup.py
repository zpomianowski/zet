"""
Py2ExeWinSetup.py

Py2Exe (version 6.6 and higher) setup file for windows/GUI programs.
Creates a single .exe file.

Simply add this file <strong class="highlight">into</strong> the same folder with the source file.
Change your source filename at the bottom to whatever you called your 
code file.  Now run Py2ExeWinSetup.py ...

Two subfolders will be created called build and dist.
The build folder is for info only and can be deleted.
Distribute whatever is in the dist folder.

The dist folder contains your .exe file, any data files you specified
in "data_files =", MSVCR71.dll and w9xpopen.exe

Your .exe file contains your code as . pyo optimized byte code files, 
all needed modules and the Python interpreter (as a Pythonxx.dll).
MSVCR71.dll can be distributed, but is often already in the system32 
folder. 
w9xpopen.exe is needed for os.popen() only, can be deleted otherwise.
"""


from distutils.core import setup
import py2exe
import sys

sys.argv.append("py2exe")
sys.argv.append("-q")

setup(
    options = {"py2exe": {"compressed": 1,
                        "optimize": 2,
                        "ascii": 1,                        
                        "bundle_files": 1,
                        "packages": ["encodings"]}},
    zipfile = None,
    # to add .dll or image files use list of filenames
    # these files are added to the dir containing the <strong class="highlight">exe</strong> file
    data_files = ["BePipe.exe", "MP4Box.exe", "libgpac.dll", "MediaInfo.dll", "aviwriter.dll", "msvcp90.dll", "xvidcore.dll", "aften.exe", "xvid_encraw.exe", "gpac_20091013.patch", "neroAacEnc.exe", "AtomicParsley.exe", "x264.exe",
                ("data", ["data/tvpw.ico"]), ("data", ["data/nothumb.png"]), ("avsplugins", ["avsplugins/QTSource.dll"]), ("avsplugins", ["avsplugins/yadif.dll"]), ("avsplugins", ["avsplugins/ffms2.dll"])],
    
    # replace 'pyg_Image3.py' with your own code filename
    # (replace windows with console for a console program)
    windows = [{"script": 'encoderGUI.pyw',            
            "icon_resources": [(1,'data/tvpw_enc.ico')]},
            {"script": 'toolGUI_tagger.py',            
            "icon_resources": [(1,'data/tvpw_enc.ico')]}]
)

import subprocess

cmd = []
#cmd.append("upx.exe -9 -o dist/tagger.exe dist/toolGUI_tagger.exe")
cmd.append("upx.exe -9 -o dist/enc.exe dist/encoderGUI.exe")

for c in cmd:
    hP = subprocess.Popen(c, shell=True)
