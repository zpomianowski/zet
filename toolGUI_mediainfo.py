# -*- encoding: utf-8 -*-

__id__ = "ZbychuMediaInfo"
__version__ = "1.0.0 BETA"
__reldate__ = "11.12.2009"
import wx
import ctypes
import MediaInfoDLL
from constants import *

ID_OPEN = 101
ID_EXIT = 103

class MediaInfo(wx.Frame):
    def __init__(self, parent, id):
        title = __id__ + " v" + __version__ + " Release: " + __reldate__
        wx.Frame.__init__(self, parent, id, title, size=(800, 600))
        self.SetIcon(wx.Icon("data/tvpw.ico", wx.BITMAP_TYPE_ICO))
          
        self.mi = None
        droptarget = FileDrop(self)
        self.SetDropTarget(droptarget)
        
        ## notebook
        self.nb = wx.Notebook(self, -1)
                   
        ## textarea
        self.text = wx.TextCtrl(self.nb, -1, style=wx.TE_READONLY | wx.TE_RICH | wx.TE_MULTILINE)
                     
        self.__setProperties()
        self.__doLayout()
        
    def __setProperties(self):        
        ## statusbar
        self.CreateStatusBar()
        self.SetStatusText(__id__ + " v" + __version__ + " Release: " + __reldate__)
        
        ## menu
        menu_file = wx.Menu()
        menu_file.Append(ID_OPEN, "&Open", "Select media file")
        menu_file.AppendSeparator()
        menu_file.Append(ID_EXIT, "&Exit", "Exit")
        
        self.Bind(wx.EVT_MENU, self.__OnOpen, id=ID_OPEN)
        self.Bind(wx.EVT_MENU, self.__OnExit, id=ID_EXIT)
        
        ## menubar
        menubar = wx.MenuBar()
        menubar.Append(menu_file, "&File")
        
        self.SetMenuBar(menubar)
        
        ## notebook
        self.nb.AddPage(self.text, "Media description")
                
              
    def __doLayout(self):
        mainframesizer = wx.BoxSizer(wx.VERTICAL)
        mainframesizer.Add(self.nb, 1, wx.EXPAND, 10)        
        self.SetSizer(mainframesizer)
        self.Layout()
        
    ## menu events
    def __OnOpen(self, event):
        dialog = wx.FileDialog (None, message='Open video file', style=wx.OPEN)
        if dialog.ShowModal() == wx.ID_OK:            
            selected = dialog.GetPath()
            self.AddFileDesc(selected)
        dialog.Destroy()
        self.nb.Refresh()
                
    def AddFileDesc(self, selected, append=False):
        self.mi = MediaInfoDLL.MediaInfo()
        self.mi.Open(selected)
        infostring = self.mi.Inform()
        self.mi.Close()
        import re   
        infostring = re.sub(r'\s{4}:', r'\t\t\t:', infostring) 
        infostring = re.sub(r'ID', r'ID\t', infostring)
        infostring = re.sub(r'Title', r'Title\t', infostring)
        infostring = re.sub(r'rtng', r'rtng\t', infostring)
        infostring = re.sub(r'\n\r', r'\n\n------------------------------------------------------------------------------------\n', infostring)
        if append == False: self.text.Clear() 
        print self.mi.Option("Info_Codecs")                   
        self.text.WriteText(infostring)
                
    def __OnExit(self, event):
        self.Destroy()

class FileDrop(wx.FileDropTarget):
    def __init__(self, parent):
        wx.FileDropTarget.__init__(self)
        self.parent = parent

    def OnDropFiles(self, x, y, filenames):
        import os
        fileList = []
        for filename in filenames:
            fileList.append(os.path.abspath(filename))
            for root, subFolders, files in os.walk(filename, topdown=False):                
                for file in files:                    
                    fileList.append(os.path.join(root,file))
        for path in fileList:
            self.parent.text.WriteText("\n==========================================")
            self.parent.text.WriteText("\nFILE: %s" % path)
            self.parent.text.WriteText("\n==========================================\n\n")
            self.parent.AddFileDesc(path, True) 
  
class MyApp(wx.App):
    def OnInit(self):
        frame = MediaInfo(None, -1)
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()