__id__ = "ZbychuThumbEditor"
__version__ = "0"
__reldate__ = "23.11.2009"
import wx
import re
from wx.media import *
from constants import *

ID_OPEN = 101
ID_SAVE = 102
ID_EXIT = 103

class ThumbEditor(wx.Frame):
    def __init__(self, parent, id):
        title = __id__ + " v" + __version__ + " Release: " + __reldate__
        wx.Frame.__init__(self, parent, id, title, size=(800, 600))
        self.SetIcon(wx.Icon("data/tvpw.ico", wx.BITMAP_TYPE_ICO))
        
        self.film = None        
        
        ## notebook
        self.nb = wx.Notebook(self, -1)
        
        ## panels
        self.vPanel = PageVideo(self.nb)
        self.bPanel = PageBitmap(self.nb)
                
        self.__setProperties()
        self.__doLayout()
        
    def __setProperties(self):        
        ## statusbar
        self.CreateStatusBar()
        self.SetStatusText(__id__ + " v" + __version__ + " Release: " + __reldate__)
        
        ## menu
        menu_file = wx.Menu()
        menu_file.Append(ID_OPEN, "&Open", "Select image to convert into thumbnail")
        menu_file.Append(ID_SAVE, "&Save", "Save thumbnail")
        menu_file.AppendSeparator()
        menu_file.Append(ID_EXIT, "&Exit", "Exit")
        
        self.Bind(wx.EVT_MENU, self.__OnOpen, id=ID_OPEN)
        self.Bind(wx.EVT_MENU, self.__OnSave, id=ID_SAVE)
        self.Bind(wx.EVT_MENU, self.__OnExit, id=ID_EXIT)
        
        ## menubar
        menubar = wx.MenuBar()
        menubar.Append(menu_file, "&File")
        
        self.SetMenuBar(menubar)
        
        ## notebook
        self.nb.AddPage(self.vPanel, "Video")
        self.nb.AddPage(self.bPanel, "Bitmap")
                
              
    def __doLayout(self):
        mainframesizer = wx.BoxSizer(wx.VERTICAL)
        mainframesizer.Add(self.nb, 1, wx.EXPAND, 10)        
        self.SetSizer(mainframesizer)
        self.Layout()
        
    ## menu events
    def __OnOpen(self, event):
        wildcard = "Video for Windows - TVPW 16:9 (*.avi)|*.avi|"
        wildcard += "Video for Windows - TVPW 4:3 (*.avi)|*.avi|"
        wildcard += "Quick Time 7 - TVPW 16:9 (*.mov)|*.mov|"
        wildcard += "Quick Time 7 - TVPW 4:3 (*.mov)|*.mov|"
        wildcard += VFORMATS
        dialog = wx.FileDialog (None, message='Open image file', wildcard=wildcard, style=wx.OPEN)
        if dialog.ShowModal() == wx.ID_OK:            
            selected = dialog.GetPath()
            id = dialog.GetFilterIndex()
            if id == 0 or id == 2: 
                AVS(selected, 2)
                self.vPanel.video.Load("play.avs")
            elif id == 1 or id == 3:                 
                AVS(selected, 1)
                self.vPanel.video.Load("play.avs")
            elif id == 9:
                AVS(selected)
                self.vPanel.video.Load("play.avs")
            else:
                self.vPanel.video.Load(selected)
        dialog.Destroy()
        self.nb.Refresh()
        
    def __OnSave(self, event):
        import sys
        sys.stderr.write("ERR: Not yet implemented!\n")
        
    def __OnExit(self, event):
        self.Destroy()
        
    ## toolbar events
    def __OnSelect(self, event):
        import sys
        sys.stderr.write("ERR: Not yet implemented!\n")
        
    def __OnCrop(self, event):
        import sys
        sys.stderr.write("ERR: Not yet implemented!\n")
        
    ## mouse events
    def __OnMouseEvt(self, event):
        if event.Dragging():
            print event.MiddleIsDown()
        
    ## paint
    def __OnPaint(self, event):
        dc = wx.PaintDC(self.panel)
        if self.img:
            dc.DrawBitmap(self.img, self.anchor[0], self.anchor[1], False)

class PageVideo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        
        ID_PLAY = 101
        ID_PAUSE = 102
        ID_STOP = 103
        ID_SHOT = 104
        
        ## reference to parent
        self.parent = parent
        
        ## video panel
        self.video = MediaCtrl(self, -1)
        self.video.ShowPlayerControls()
        
        ## ctrl panel
        self.ctrlp = wx.Panel(self, -1)
        self.shotb = wx.Button(self.ctrlp, ID_PLAY, "Snapshot")
        
        self.__setProperties()
        self.__doLayout()
        
    def __setProperties(self):
        pass
        
    def __doLayout(self):
        ctrlsizer = wx.BoxSizer(wx.HORIZONTAL)
        ctrlsizer.Add(self.shotb, 1, wx.EXPAND, 10)
        self.ctrlp.SetSizer(ctrlsizer)
        self.ctrlp.Layout()
        
        mainsizer = wx.BoxSizer(wx.VERTICAL)
        mainsizer.Add(self.video, 1, wx.EXPAND, 10)
        mainsizer.Add(self.ctrlp, 0, wx.EXPAND, 10)
        self.SetSizer(mainsizer)
        self.Layout()
        
class PageBitmap(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

class AVS:
    def __init__(self, path, flag=0):
        ##format searcher
        self.filter = re.compile(r"(.+?)(\.([^\.]*))?$")
        format = self.filter.search(path).group(3)
        
        ##settings depended on profiles
        filmpath = path
        if flag == 1:
            size = (768, 576)
        elif flag == 2:
            size = (1024, 576)
        
        self.path = "play.avs"
        # QuickTime 
        if format == "mov":
            script = "LoadPlugin(\"avsplugins/QTSource.dll\")\n" 
            script += "QTInput(\"" + filmpath + "\")\n"
        # Anything else
        else:
            script = "DirectShowSource(\"" + filmpath + "\")\n"
        if flag != 0: script += "BicubicResize(" + str(size[0]) + ", " + str(size[1]) + ", 0, 0.4)\n"
        script += "ConvertToYV12()\n"
                
        ##saving script        
        self.f = open(self.path, 'w')        
        try:
            self.f.write(script)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.f.close()         

class MyApp(wx.App):
    def OnInit(self):
        frame = ThumbEditor(None, -1)
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()