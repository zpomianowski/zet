import os
import wx
from propSplitter import PropSplitter
from tabGUI import Grid
from cPanelGUI import cPanel
from constants import *

## tools
import toolGUI_tagger
#import toolGUI_thumb
import toolGUI_mediainfo
#import toolGUI_playcap

##edition
if EDITION == 1: from Personal import *

""" GENERAL INFORMATION """
__id__ = ZETid
__author__ = ZETauthor
__copyright__ = ZETcopyright
__licence__ = ZETlicence
__version__ = ZETversion 
__reldate__ = ZETreldate

ID_OPEN = 101
ID_EXIT  = 102
ID_FTP = 103
ID_ABOUT = 104
ID_TAG = 105
ID_THUMB = 106
ID_MI = 107
ID_EMPTYOUTPUT = 108
ID_PERSONAL = 109
ID_PLAYCAP = 110
ID_OPENCONT = 111

class MyFrame(wx.Frame):
    def __init__(self, parent, ID, title):
        ## essential initializations
        self.grid = None
        
        wx.Frame.__init__(self, parent, ID, title, wx.DefaultPosition, wx.Size(960, 300)) 
        self.SetIcon(wx.Icon("data/tvpw.ico", wx.BITMAP_TYPE_ICO))
                        
        self.__setProperties()
        self.__doLayout()
        #setPriority()
        
        if EDITION == 1:
            loadPersonalData()
                          
    def __setProperties(self):        
        ## statusbar
        self.CreateStatusBar()
        self.SetStatusText(__copyright__)

        ## menu
        menu = wx.Menu()
        menu.Append(ID_OPEN, "&Open", "Select video clips to encode")
        menu.Append(ID_OPENCONT, "&Open output folder", "View encoded files")        
        menu.AppendSeparator()
        menu.Append(ID_EMPTYOUTPUT, "&Empty output folder", "List all files in output folder and delete'em all")
        menu.AppendSeparator()
        menu.Append(ID_EXIT, "E&xit", "Terminate the program")
    
        self.Bind(wx.EVT_MENU, self.__OnOpen, id=ID_OPEN)
        self.Bind(wx.EVT_MENU, self.__OnOpenCont, id=ID_OPENCONT)
        self.Bind(wx.EVT_MENU, self.__OnExit, id=ID_EXIT)
        self.Bind(wx.EVT_MENU, self.__OnEmptyOutput, id=ID_EMPTYOUTPUT)
        
        if EDITION == 0:
            menu2 = wx.Menu()
            menu2.Append(ID_TAG, "Edit &metadata/artwork", "Change metadata in Ur mp4 file")
            menu2.Append(ID_MI, "Media info", "See detailed video/music/image file description")
            #menu2.Append(ID_THUMB, "Edit T&humbnail", "Resize/Crop image for thumbnail")
            #menu2.Append(ID_PLAYCAP, "PlayCap", "Show video stream from connected camcorder")
            
            self.Bind(wx.EVT_MENU, self.__OnTag, id=ID_TAG)
            #self.Bind(wx.EVT_MENU, self.__OnThumb, id=ID_THUMB)   
            self.Bind(wx.EVT_MENU, self.__OnMI, id=ID_MI) 
            #self.Bind(wx.EVT_MENU, self.__OnPlayCap, id=ID_PLAYCAP)
            
            menu3 = wx.Menu()    
            menu3.Append(ID_FTP, "&Ftp connection settings")
            
            self.Bind(wx.EVT_MENU, self.__OnFtp, id=ID_FTP)
            
        if EDITION == 1:
            menu3 = wx.Menu()    
            menu3.Append(ID_PERSONAL, "&Set Your Personal Data")
            
            self.Bind(wx.EVT_MENU, self.__OnPersonal, id=ID_PERSONAL)
        
        menu4 = wx.Menu()    
        menu4.Append(ID_ABOUT, "&About", "Only cute girls can write e-mails to me. Am i clear?;]")
                
        self.Bind(wx.EVT_MENU, self.__OnAbout, id=ID_ABOUT)
                         
        ## menubar
        menuBar = wx.MenuBar()
        menuBar.Append(menu, "&File")
        if EDITION == 0: menuBar.Append(menu2, "&Tools")
        if EDITION == 0: menuBar.Append(menu3, "&Ftp")
        if EDITION == 1: menuBar.Append(menu3, "&Personal Data")
        menuBar.Append(menu4, "&About")
        
        self.SetMenuBar(menuBar)
        
        ## splitter
        self.split = PropSplitter(self, -1, 0.50) 
        
        ## tab        
        self.grid = Grid(self.split)
        self.Bind(wx.EVT_SIZE, self.__OnSize)
        
        ## command panel
        self.cpanel = cPanel(self.split, self.grid, -1)       
        
        ## output folder
        if not os.path.exists("output"):
                os.mkdir("output")
                
        ## override events
        self.Bind(wx.EVT_CLOSE, self.__OnClose)
            
    def __doLayout(self):
        self.split.SplitHorizontally(self.grid, self.cpanel)   
        mainframesizer = wx.BoxSizer(wx.VERTICAL)
##        mainframesizer.Add(self.grid, 1, wx.EXPAND, 10)        
##        mainframesizer.Add(self.cpanel, 0, wx.EXPAND, 10)
        mainframesizer.Add(self.split, 1, wx.EXPAND, 10)
        self.SetSizer(mainframesizer)
        self.Layout()
        self.Centre()
        
    def __OnOpen(self, event):        
        dialog = wx.FileDialog (None, message='Open video files to add to list...', wildcard=VFORMATS, style=wx.OPEN | wx.MULTIPLE)
        if dialog.ShowModal() == wx.ID_OK:            
            selected = dialog.GetPaths()            
            for path in selected:    
                self.grid.AppendRows(1, True) 
                self.grid.AddItem(path)            
            self.grid.CalibrateSize() 
            self.Layout()                  
        dialog.Destroy()
        
    def __OnOpenCont(self, event):
        import subprocess        
        subprocess.Popen("explorer " + "\"" + os.getcwd() + "\\output\"")
    
    def __OnEmptyOutput(self, event):
        dial = wx.MessageDialog(self, 'Are U sure U want to delete all files in output folder?', 'Warning?', 
            wx.YES_NO | wx.NO_DEFAULT | wx.ICON_EXCLAMATION)
        if dial.ShowModal() == wx.ID_YES:        
            filelist = os.listdir("output")
            for file in filelist:
                os.remove("output/" + file)
                print "INFO: File \"%s\" has been deleted!" % (file)
                
    def __OnExit(self, event):
        if self.cpanel.flag == True:
            target=self.cpanel.stop()
            chlist = self.GetChildren()
            for ch in chlist:               
                ch.Close()
            self.Destroy()   
        else:
            self.Destroy()
    
    def __OnFtp(self, event):
        self.cpanel.ftpsender.defineSettings()
        
    def __OnPersonal(self, event):
        setPersonalData()
    
    def __OnAbout(self, event):        
        string = \
            __id__ + " version " + __version__ + \
            "\nLast update: " + __reldate__ + \
            "\n\nBy: " + __author__ + \
            "\nContact: zpomianowski@tvpw.pl" + \
            "\n\n" + __copyright__ + \
            "\n" + __licence__                       
            
        dial = wx.MessageDialog(self, string, 'About', 
            wx.YES_DEFAULT | wx.ICON_INFORMATION)
        dial.ShowModal() 
            
    def __OnSize(self, event):
        if not self.grid:
            return
        self.grid.CalibrateSize()
        self.Layout()
        
    def __OnTag(self, event):
        print "INFO: Editing tags..."
        tagger = toolGUI_tagger.Tagger(self, -1)
        tagger.Show(True)        
        
    def __OnThumb(self, event):
        #print "INFO: Editing thumbnail..."
        #thumbeditor = toolGUI_thumb.ThumbEditor(self, -1)
        #thumbeditor.Show(True)
        pass        
        
    def __OnMI(self, event):
        print "INFO: Viewing detailed media file description..."
        mi = toolGUI_mediainfo.MediaInfo(self, -1)
        mi.Show(True)        
        
    def __OnPlayCap(self, event):
        #print "INFO: Capturing video stream from camcorder..."
        #playcap = toolGUI_playcap.PlayCap(self, -1)
        #playcap.Show(True)
        pass        
        
    def __OnClose(self, event):
        if self.cpanel.flag == True:               
            self.Iconize()                                                 
        else:            
            self.Destroy()
    
def setPriority(pid=None,priority=0):
    import platform as pl    
    print "OS: %s, Release: %s, Version: %s - (%s)" % (pl.system(), pl.release(), pl.version(), pl.platform())
    print "Architecture: bits = %s, linkage = %s" % (pl.architecture()[0], pl.architecture()[1])
    print "Machine: %s, Node: %s, Processor: %s" % (pl.machine(), pl.node(), pl.processor()) 
    ## Windows
    if pl.system() == "Windows":
        """ Set The Priority of a Windows Process.  Priority is a value between 0-5 where
            2 is normal priority.  Default sets the priority of the current
            python process but can take any valid process ID. """
            
        import win32api,win32process,win32con
        
        priorityclasses = [win32process.IDLE_PRIORITY_CLASS,
                           win32process.BELOW_NORMAL_PRIORITY_CLASS,
                           win32process.NORMAL_PRIORITY_CLASS,
                           win32process.ABOVE_NORMAL_PRIORITY_CLASS,
                           win32process.HIGH_PRIORITY_CLASS,
                           win32process.REALTIME_PRIORITY_CLASS]
        if pid == None:
            pid = win32api.GetCurrentProcessId()
            handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
            win32process.SetPriorityClass(handle, priorityclasses[priority])
   
class MyApp(wx.App):
    def OnInit(self):  
        frame = MyFrame(None, -1, "ZbychuEncoderTVPW (ZET) v" + __version__)
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
