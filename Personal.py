import wx
from constants import *
import ConfigParser
    
class PersonalDiag(wx.Dialog):
    def __init__(self, parent, id, title):
        wx.Dialog.__init__(self, parent, id, title, size=(300, 230))

        panel = wx.Panel(self, -1)
        
        wx.StaticBox(panel, -1, 'Personal Data', (5, 5), (285, 150))
        wx.StaticText(panel, -1, 'Name:', (15, 30), style=wx.RB_GROUP)
        self.name = wx.TextCtrl(panel, -1, '', (80, 28), (200, 18))
        wx.StaticText(panel, -1, 'Surname:', (15, 60), style=wx.RB_GROUP)
        self.surname = wx.TextCtrl(panel, -1, '', (80, 58), (200, 18), style=wx.RB_GROUP)
        wx.StaticText(panel, -1, 'Email:', (15, 90), style=wx.RB_GROUP)
        self.email = wx.TextCtrl(panel, -1, '', (80, 88), (200, 18))
        
        okeyButton = wx.Button(self, 101, 'Ok', size=(70, 30))
        cancButton = wx.Button(self, 102, 'Cancel', size=(70, 30))
        self.SetAffirmativeId(101)
        self.SetEscapeId(102)
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(okeyButton, 1)
        hbox.Add(cancButton, 1, wx.LEFT, 5)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(panel)
        vbox.Add(hbox, 1, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, 10)

        self.SetSizer(vbox)                 

def loadPersonalData():
    import ConfigParser
    config = ConfigParser.RawConfigParser()
    config.read(cfgfile)
    
    try:
        ## load settings
        personaldata = "%s %s;%s" % \
            (config.get('personal_data', 'name'), \
            config.get('personal_data', 'surname'), \
            config.get('personal_data', 'email'))
    except Exception:
        diag = PersonalDiag(None, -1, 'First run...')
        if diag.ShowModal() == 101:
            name = diag.name.GetValue()
            surname = diag.surname.GetValue()
            email = diag.email.GetValue()
           
            config = ConfigParser.RawConfigParser()
            config.read(cfgfile)
            try:
                config.add_section('personal_data')
            except Exception:
                pass
            
            config.set('personal_data', 'name', name)
            config.set('personal_data', 'surname', surname)
            config.set('personal_data', 'email', email)
            
            with open(cfgfile, 'wb') as configfile:
                config.write(configfile)
                
            ## load new settings
            personaldata = "%s %s;%s" % \
                (config.get('personal_data', 'name'), \
                config.get('personal_data', 'surname'), \
                config.get('personal_data', 'email'))
                      
        diag.Destroy()
    return personaldata
        
def setPersonalData():
    config = ConfigParser.RawConfigParser()
    config.read(cfgfile)
    
    diag = PersonalDiag(None, -1, 'Change Your personal data')
    
    try:
        ## load settings
        name = config.get('personal_data', 'name')
        surname = config.get('personal_data', 'surname')
        email = config.get('personal_data', 'email')
        diag.name.SetValue(name)
        diag.surname.SetValue(surname)
        diag.email.SetValue(email)
        personaldata = "%s %s;%s" % \
                (config.get('personal_data', 'name'), \
                config.get('personal_data', 'surname'), \
                config.get('personal_data', 'email'))
    except Exception:
        sys.stderr.write("ERR: Error - couldn't load personal data!\n")
    
    if diag.ShowModal() == 101:
        name = diag.name.GetValue()
        surname = diag.surname.GetValue()
        email = diag.email.GetValue()
       
        config = ConfigParser.RawConfigParser()
        config.read(cfgfile)
        try:
            config.add_section('ftp')
        except Exception:
            pass
        
        config.set('personal_data', 'name', name)
        config.set('personal_data', 'surname', surname)
        config.set('personal_data', 'email', email)
        
        with open(cfgfile, 'wb') as configfile:
            config.write(configfile)
            
        ## load new settings
        personaldata = "%s %s;%s" % \
                (config.get('personal_data', 'name'), \
                config.get('personal_data', 'surname'), \
                config.get('personal_data', 'email'))
                      
    diag.Destroy()
    return personaldata