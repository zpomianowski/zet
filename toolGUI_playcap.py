__id__ = "ZbychuPlayCap"
__version__ = "0"
__reldate__ = "10.02.2010"
import wx
from constants import *

import sys

# import the necessary things for OpenCV
from opencv import cv
from opencv import highgui

# the codec existing in cvcapp.cpp,
# need to have a better way to specify them in the future
# WARNING: I have see only MPEG1VIDEO working on my computer
H263 = 0x33363255
H263I = 0x33363249
MSMPEG4V3 = 0x33564944
MPEG4 = 0x58564944
MSMPEG4V2 = 0x3234504D
MJPEG = 0x47504A4D
MPEG1VIDEO = 0x314D4950
AC3 = 0x2000
MP2 = 0x50
FLV1 = 0x31564C46

class PlayCap(wx.Frame):
    TIMER_PLAY_ID = 101
    def __init__(self, parent, id):
        title = __id__ + " v" + __version__ + " Release: " + __reldate__
        wx.Frame.__init__(self, parent, id, title, size=(800, 600))
        self.displayPanel = wx.Panel(self, -1)
        self.SetIcon(wx.Icon("data/tvpw.ico", wx.BITMAP_TYPE_ICO))
        
        self.device = 0
        self.frame_size = None
        self.capture = highgui.cvCreateCameraCapture(self.device)
                
        if not self.capture:
            import sys
            sys.stderr.write("ERR: Couldn't find camcorder\n")
            self.Destroy()
        else:
            frame = highgui.cvQueryFrame(self.capture)
            self.bmp = wx.BitmapFromBuffer(frame.width, frame.height, frame.imageData)
            self.Bind(wx.EVT_PAINT, self.__OnPaint)
            self.playTimer = wx.Timer(self, self.TIMER_PLAY_ID)
            wx.EVT_TIMER(self, self.TIMER_PLAY_ID, self.__OnNextFrame)
            self.playTimer.Start(1000/25)
            
            self.frame_size = cv.cvGetSize(frame)
            
            self.__setProperties()
            self.__doLayout()
                       
            
            self.writer = highgui.cvCreateVideoWriter("test.avi", highgui.CV_FOURCC('M', 'J', 'P', 'G'), 25, self.frame_size, 1)#
                    
            self.Show(True)
        
    def __setProperties(self):        
        ## statusbar
        self.CreateStatusBar()
        self.SetStatusText("Frame size: %dx%d" % (self.frame_size.width, self.frame_size.height))
                     
    def __doLayout(self):
        mainframesizer = wx.BoxSizer(wx.VERTICAL)
        mainframesizer.Add(self.displayPanel, 1, wx.SHAPED, 10)        
        self.SetSizer(mainframesizer)
        self.Layout()
        pass
        
    def __OnPaint(self, evt):  
        if self.bmp:
            dc=wx.BufferedPaintDC(self.displayPanel, self.bmp)
        evt.Skip()
        
    def __OnNextFrame(self, evt):
        frame = highgui.cvQueryFrame(self.capture)
        if frame:
            highgui.cvWriteFrame(self.writer, frame)
            cv.cvCvtColor(frame, frame, cv.CV_BGR2RGB)
            self.bmp.CopyFromBuffer(frame.imageData)            
            self.Refresh()
        evt.Skip()
     
class MyApp(wx.App):
    def OnInit(self):
        frame = PlayCap(None, -1)
        self.SetTopWindow(frame)
        return True

if __name__ == '__main__':
    app = MyApp(0)
    app.MainLoop()
