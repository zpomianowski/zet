# -*- encoding: utf-8 -*-

import sys
import ctypes
import md5

def test():
    import MediaInfoDLL
    mi = MediaInfoDLL.MediaInfo()
    s = "test.mp4"
    s = str(s.encode('utf-16-le'))
    print mi.Open(s)
    print mi.Get("Stream_Video", 0, u"Interlacement")
    mi.Close()

def sumfile(fobj):
    m = md5.new()
    while True:
        d = fobj.read(8096)
        if not d:
            break
        m.update(d)
    return m.hexdigest()

def md5sum(fname):
    try:
        f = file(fname, 'rb')
    except:
        return 'Failed to open file'
    ret = sumfile(f)
    f.close()
    print "o" + ret

if __name__ == '__main__':
    print "jedziemy..."
    md5sum("test_1111.mp4")