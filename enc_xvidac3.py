import os
import sys
import subprocess
import threading
import re
import time
from constants import *

""" 
Module, to proper work needs:
Encoder:
__init__(self, parent, id, film)
start(self)
stop(self)
clear(self)
"""

class Encoder:
    def __init__(self, parent, id, film):
        self.hP = None
        self.bat = None
        self.parent = parent             
        self.film = film 
        self.id = id
        ## diagnostic parameters
        self.dia_video = 0
        self.dia_audio = 0
        self.dia_mp4box = 0 
        self.dia_ap = 0 
        ## control
        self.encstatus = 0
        
    def start(self):
        info = "Please wait..." 
        
        ## video
        self.bat = BATENCV(self, self.film) 
        
        if self.encstatus < 0:
            return self.encstatus
             
        print self.getTime() + " ENC: Encoding video..."      
        cmd = ".bat"        
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, env=os.environ, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        xvid = xvidmonitor(self.film)        
        while self.hP.poll() is None: 
            string = self.hP.stdout.readline()
            #sys.stdout.write(string)           
            self.dia_video, info = xvid.getInfo(string)                           
            ## set diagnistics info            
            total = 0.9*self.dia_video+0.08*self.dia_audio+0.02*self.dia_mp4box
            progress = "Progress: %0.2f%s - %s" % (total, "%", info)
            self.parent.parent.SetTitle(progress) 
            self.parent.parent.SetStatusText(progress)            
            self.parent.gauge_task.SetValue(total)
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Fatal error - couldn't encode video\n")
            self.encstatus = -1
            return self.encstatus
        if self.encstatus == 1:
            return 1
        self.dia_video = 100
        
        ## audio
        self.bat = BATENCA(self.film)   
        if self.runflag == False:
            print self.getTime() + " ENC: Quit audio encoding..."
            return    
        print self.getTime() + " ENC: Encoding audio..."      
        cmd = ".bat"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        nero = neromonitor()        
        while self.hP.poll() is None: 
            string = self.hP.stdout.read(100)                           
            self.dia_audio, info = nero.getInfo(string)     
            ## set diagnistics info            
            total = 0.9*self.dia_video+0.09*self.dia_audio+0.01*self.dia_mp4box
            progress = "Progress: %0.2f%s - %s" % (total, "%", info)
            self.parent.parent.SetTitle(progress) 
            self.parent.parent.SetStatusText(progress)            
            self.parent.gauge_task.SetValue(total)
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Fatal error - couldn't encode audio\n")
            self.encstatus = -2
            return self.encstatus
        if self.encstatus == 1:
            return 1
        self.dia_audio = 100
        
        ## muxing
        self.bat = BATENCM(self.film) 
        if self.runflag == False:
            print self.getTime() + " ENC: Quit muxing..."
            return       
        print self.getTime() + " ENC: Muxing video and audio, creating ISO file..."      
        cmd = ".bat"
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW
        self.hP = subprocess.Popen(cmd, shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, startupinfo=startupinfo)
        mp4box = mp4boxmonitor()        
        while self.hP.poll() is None: 
            string = self.hP.stdout.read(100)                           
            info = mp4box.getInfo(string)       
            ## set diagnistics info            
            total = 0.9*self.dia_video+0.09*self.dia_audio+0.01*self.dia_mp4box
            progress = "Progress: %0.2f%s - %s" % (total, "%", info)
            self.parent.parent.SetTitle(progress) 
            self.parent.parent.SetStatusText(progress) 
        ##error detection
        if self.hP.poll() == -1:
            sys.stderr.write(self.getTime() + " ENC: Fatal error - couldn't mux video and audio\n")
            self.encstatus = -3
            return self.encstatus
        if self.encstatus == 1:
            return 1
        self.parent.gauge_task.SetValue(100)
        
        ## finishing
        timestring = str(time.localtime()[0]) + "." + \
                    str(time.localtime()[1]).zfill(2) + "." + \
                    str(time.localtime()[2]).zfill(2) + " [" + \
                    str(time.localtime()[3]).zfill(2) + ":" + \
                    str(time.localtime()[4]).zfill(2) + ":" + \
                    str(time.localtime()[5]).zfill(2) + "]"             
        self.parent.parent.SetTitle("Progress: 100% - Finished - " + timestring) 
        self.parent.parent.SetStatusText("Progress: 100% - Finished - " + timestring) 
        print self.getTime() + " ENC: Done"    
        
        return self.encstatus        
    
    def getTime(self):
        timestring = "[" + str(time.localtime()[0]) + "." + \
                        str(time.localtime()[1]).zfill(2) + "." + \
                        str(time.localtime()[2]).zfill(2) + " " + \
                        str(time.localtime()[3]).zfill(2) + ":" + \
                        str(time.localtime()[4]).zfill(2) + ":" + \
                        str(time.localtime()[5]).zfill(2) + "]"    
        return timestring
        
    def stop(self):
        self.encstatus = 1  
        if self.hP != None:
            try:
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess._subprocess.STARTF_USESHOWWINDOW                
                handle = subprocess.Popen("taskkill /F /T /PID %i" % self.hP.pid , shell=False, startupinfo=startupinfo)
                handle.wait()
                print self.getTime() + " ENC: Process terminated!"
            except Exception:
                sys.stderr.write(self.getTime() + " ENC: Error - couldn'terminate - Windows sucks;]\n")
         
    def clear(self):
        time.sleep(1)
        try:
            if os.path.exists('.bat'):
                os.remove('.bat')
            if os.path.exists('.avs'):
                os.remove('.avs')
            if os.path.exists('output\\.stats.temp'):
                os.remove('output\\.stats.temp')
            if os.path.exists('output\\.stats'):
                os.remove('output\\.stats')
            film = "output\\%s.avi" % (self.film['name'])    
            video = "output\\%s_video.m4v" % (self.film['name'])
            audio = "output\\%s_audio.ac3" % (self.film['name'])
            if os.path.exists(film):
                if os.path.exists(video):
                    os.remove(video)
                if os.path.exists(audio):
                    os.remove(audio)
            else:
                sys.stderr.write("ENC: Error - no result file found\n")
            print self.getTime() + " ENC: Unneeded files cleaned"
        except Exception:
            sys.stderr.write(self.getTime() + " ENC: Error - couldn't delete trash files\n")

class xvidmonitor:
    def __init__(self, film):
        self.parser = re.compile(r'(\d{1,}):\skey')
        self.framescount = film['framescount']
        self.p = 0
        self.tempp = 0
        self.f = 0
        self.passes = None
        self.currentpass = 1        
        if False:
            self.passes = 2
        else:
            self.passes = 1
    
    def getInfo(self, string):        
        info = "Please wait..."       
        dia_v = 0
        tup = self.parser.search(string)                 
        if tup:
            self.f = int(tup.group(1))
        if self.passes == 1:
            dia_v = float(self.f)/self.framescount * 100                        
        if self.passes == 2:            
            if self.currentpass == 1:                
                dia_v = self.p/2 
                if self.p < self.tempp:
                    self.currentpass = 2
                self.tempp = float(self.f)/self.framescount * 100
            if self.currentpass == 2:
                dia_v = 50+self.p/2                
        info = \
            "Video: %i%s Frames: %i/%i" % (dia_v, "%", self.f, self.framescount)   
        #print dia_v
        return dia_v, info
   
class neromonitor:
    def __init__(self):
        self.parser = re.compile(r'(\d{,2},\d{13})%')
        self.p = 0
        self.dia_a = 0
    
    def getInfo(self, string):        
        info = "Please wait..."              
        tup = self.parser.search(string)                         
        if tup:            
            self.p = float(re.sub(r',', r'.', tup.group(1)))
            if self.p > self.dia_a:
                self.dia_a = self.p            
        info = "Audio: %i%s" % (self.dia_a, "%")        
        return self.dia_a, info
 
class mp4boxmonitor:
    def __init__(self):
        self.parser = re.compile(r'(ISO\sFile\sWriting:\s\|=*\s*\|)')
        self.p = 0
    
    def getInfo(self, string):        
        info = "Please wait..." 
        dia_a = "|"      
        tup = self.parser.search(string)                         
        if tup:            
            self.p = tup.group(1)
            dia_a = self.p
            info = "Bulding file: %s" % (self.p)        
        return info
           
class AVS:
    def __init__(self, film):
        ##format searcher
        self.filter = re.compile(r"(.+?)(\.([^\.]*))?$")
        format = self.filter.search(film['path']).group(3).lower()

        ##frames count
        self.autodetectFramesCount(film)
        
        ##aspectratio autodetect
        if film['aspectr'] == ASPECTR[0]:
            self.autodetectDAR(film)
        
        path = film['path']
        size = None
        if film['aspectr'] == ASPECTR[2]:
            size = (768, 576)
        elif film['aspectr'] == ASPECTR[1]:
            size = (1024, 576)
        if not size:
            size = (1024, 576)
            sys.stderr.write("WARN: Uncommon Display Aspect Ratio detected\n")
            sys.stderr.write("WARN: Result file might have wrong Display Aspect Ratio\n")
        print "ENC: Resolution changed to: %i x %i" % (size[0], size[1])
        
        self.path = ".avs"
        # QuickTime 
        if format == "mov":
            script = "LoadPlugin(\"avsplugins/QTSource.dll\")\n" 
            script += "QTInput(\"" + path + "\")\n"
        elif format == "wmv":
            print "Clip will be encoded through DirectShowSource"
            script = "DirectShowSource(\"" + path + "\")\n"    
        # Anything else
        else:
            print "Clip will be encoded through FFmpegSource2 plugin"
            script = "LoadPlugin(\"avsplugins/ffms2.dll\")\n"            
        script += "BicubicResize(" + str(size[0]) + ", " + str(size[1]) + ", 0, 0.75)\n"
        script += "ConvertToYV12()\n"
                
        ##saving script        
        self.f = open(self.path, 'w')        
        try:
            self.f.write(script)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.f.close()  
        
    def autodetectDAR(self, film): 
        import MediaInfoDLL
        self.mi = MediaInfoDLL.MediaInfo()
        self.mi.Open(film['path'])
        aspectr = self.mi.Get("Stream_Video", 0, u"DisplayAspectRatio_Original/String")
        if aspectr:
            print "ENC: Detected %s Original Display Aspect Ratio" % (aspectr)
            film['aspectr'] = aspectr
        else:
            aspectr = self.mi.Get("Stream_Video", 0, u"DisplayAspectRatio/String")
            if aspectr:
                print "ENC: Detected %s Display Aspect Ratio" % (aspectr)
                film['aspectr'] = aspectr
            else:                
                sys.stderr.write("WARN: Couldn't establish Display Aspect Ratio\n") 
                sys.stderr.write("WARN: Default chosen: %s\n") % (ASPECTR[1])
        self.mi.Close()
        
    def autodetectFramesCount(self, film):
        import MediaInfoDLL
        self.mi = MediaInfoDLL.MediaInfo()
        self.mi.Open(film['path'])
        film['framescount'] = int(self.mi.Get("Stream_Video", 0, u"FrameCount"))        
        self.mi.Close()        
        
class BATENCV:
    def __init__(self, parent, film):        
        self.avs = AVS(film)        
        self.cmd = None    
                            
        self.generateCMD(film)
        import codecs
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
            parent.encstatus = -6
            parent.clear()
        self.bat.close()           
    
    def generateCMD(self, film): 
        #XVID + " -i %s -type 2 -qpel -single -bitrate 6000 -o \"output\\%s_video.m4v" % (self.avs.path, film['name'])
        self.cmd = XVID + " -i %s -stats -type 2 -bitrate 4000 -kboost 100 -chigh 30 -clow 15 -overhead 0 -turbo -max_key_interval 250 -nopacked -vhqmode 4 -qpel -closed_gop -lumimasking -imin 1 -pmin 1 -bvhq -bquant_ratio 162 -bquant_offset 0 -bmin 1 -threads 4  -o \"output\\%s_video.m4v" % (self.avs.path, film['name']) 
                    
class BATENCA:
    def __init__(self, film):        
        self.avs = AVS(film)        
        self.cmd = None    
        
        self.generateCMD(film)
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.bat.close()           
    
    def generateCMD(self, film):
        q = 250 
        ##format searcher
        self.filter = re.compile(r"(.+?)(\.([^\.]*))?$")
        format = self.filter.search(film['path']).group(3).lower()
                
        if format == "wmv":
            self.cmd = \
            BEPIPE + " --script \"DirectShowSource(^%s^)\" | " % film['path'] + MP3 + " - -q %i \"output\\%s_audio.ac3\"" % (q, film['name'])
        else:                        
            self.cmd = \
            BEPIPE + " --script \"FFAudioSource(^%s^)\" | " % film['path'] + MP3 + " - -q %i \"output\\%s_audio.ac3\"" % (q, film['name'])
        
class BATENCM:
    def __init__(self, film):
        self.cmd = None    
        
        self.generateCMD(film)
        self.bat = open('.bat', 'w')
        try:
            self.bat.write(self.cmd)
        except Exception:
            sys.stderr.write("ERR: Don't use polish charset\n")
        self.bat.close()           
    
    def generateCMD(self, film):        
        self.cmd = \
        MP4BOX + " -new -add \"output\\%s_video.m4v\" -add \"output\\%s_audio.ac3\" \"output\\%s.avi\"" % (film['name'], film['name'], film['name'])
        
        
