EDITION = 0 ## 0-enterprise 1-standard
""" GENERAL INFORMATION """
ZETid = "ZbychuEncoderTVPW (ZET)"
ZETauthor = "Zbigniew Pomianowski"
ZETcopyright = "2009 Studencka Telewizja Internetowa TVPW"
ZETlicence = "CC"
if EDITION == 0: ZETversion = "1.1.16 BETA Enterprise Edition" 
if EDITION == 1: ZETversion = "1.1.16 BETA"
ZETreldate = "23.07.2011"
# apps
X264 = None
NERO = None
BEPIPE = None
MP4BOX = None
AP = None

import platform
import os
if platform.system() == "Windows":
    ##apps
    X264 = "x264.exe"
    NERO = "neroAacEnc.exe"
    BEPIPE = "BePipe.exe"
    MP4BOX = "MP4Box.exe"
    AP = "AtomicParsley.exe"
    XVID = "xvid_encraw.exe"
    MP3 = "aften.exe"
    
#formats
VFORMATSLIST = ("avi", "mpeg", "ts", "wmv", "mov", "mp4", "mkv", "rm", "3gp", "ogg", "asf")
VFORMATS = "Video for Windows (*.avi)|*.avi|" + \
    "Material eXchange Format (*.mxf)|*.mxf|" + \
    "Moving Picture Experts Group (*.mpeg)|*.mpeg|" + \
    "MPEG Transport Stream (*.ts)|*.ts|" + \
    "Windows Media Video (*.wmv)|*.wmv|" + \
    "H.264/MPEG-4 AVC (*.mp4)|*.mp4|" + \
    "Quick Time 7 (*.mov)|*.mov|" + \
    "Matroska (*.mkv)|*.mkv|" + \
    "RealMedia Variable Bit Rate - generates async btw vid and audio (*.rmvb)|*.rmvb|" + \
    "RealMedia (*.rm)|*.rm|" + \
    "3GP - not tested (*.3gp)|*.3gp|" + \
    "OGG - not tested (*.ogg)|*.ogg|" + \
    "Advanced Systems Format - not tested (*.asf)|*.asf|" + \
    "All files (*.*)|*.*"

GFORMATS = "JPEG (*.jpg)|*.jpg|" + \
    "Portable Network Graphics (*.png)|*.png|" + \
    "Bitmap (*.bmp)|*.bmp"              
                
#parameters
if EDITION == 0: ENCODING = ("avc aac", "xvid ac3")
if EDITION == 1: ENCODING = ("avc aac",)
THUMB = ("no thumb", "chosen")
APL = ("clean", "explicit")    
ASPECTR = ("auto", "16:9", "4:3")
PROFILES = ("publish (HD-1080p)", "publish (HD-720p)", "publish (SD-576p)", "youtube (SD-576)", "publish (MediumQ-336p)", "preview (SD-576p)", "shit2mp4")
DEINTERLACE = ("auto", "yadif", "no")
if EDITION == 1: PROFILES_1 = ("publish (HD-1080p)", "publish (HD-720p)", "publish (SD-576p)", "publish (MediumQ-336p)")
FTPSEND = ("no", "yes")
if EDITION == 1: FTPSEND_1 = ("yes",)
STATES = ("idle", "waiting", "encoding", "done", "sent", "not sent", "warning", "error", "aborted")
 
#Columns IDs
C_PATH = 0
C_SIZE = 1
C_NAME = 2
C_THUMB = 3
C_APL = 4
C_ASPECT = 5
C_PROFILE = 6
C_DEINTERLACE = 7
C_FTP = 8
C_STATE = 9
C_ENC = 10

## Config file
if EDITION == 0: cfgfile = 'config.zet'
if EDITION == 1: cfgfile = 'config_1.zet'
 
