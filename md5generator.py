# -*- encoding: utf-8 -*-

import sys
import md5
from threading import Thread

class md5gen(Thread):
   def __init__ (self, fname):
      Thread.__init__(self)
      self.fname = fname
      self.status = -1
      self.value = None
   def run(self):
      self.value = md5sum(self.fname)

def sumfile(fobj):
    m = md5.new()
    while True:
        d = fobj.read(8096)
        if not d:
            break
        m.update(d)
    return m.hexdigest()

def md5sum(fname):
    print "ChSG: cumputing MD5 started..."
    try:
        f = file(fname, 'rb')
    except:
        return "ChecksumGen: Failed to open file"
    ret = sumfile(f)
    f.close()
    print "ChSG: " + ret.upper()
    print "ChSG: thread finished"
    return ret

if __name__ == '__main__':
    print "jedziemy..."
    gen = md5gen("F:\\Filmy TVPW\\20100411_WonderfulTown.avi")
    gen.start()
    print "czekamy..."