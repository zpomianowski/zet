import wx
import wx.grid
import time
import re
from constants import *

class Grid(wx.grid.Grid):
    def __init__(self, parent):
        #self.filter = re.compile(r'(2\d{7}|PREVIEW|)(_.*)')        
        wx.grid.Grid.__init__(self, parent, -1)        
        self.SetDefaultEditor(wx.grid.GridCellAutoWrapStringEditor())        
        droptarget = FileDrop(self)
        self.SetDropTarget(droptarget)
        ## appearance        
        self.SetLabelFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL ,wx.FONTWEIGHT_BOLD))    
        self.SetDefaultCellFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL ,wx.FONTWEIGHT_NORMAL))
        
        self.SetColLabelSize(20)
        self.SetRowLabelSize(50)
        self.SetRowLabelAlignment(wx.ALIGN_RIGHT, wx.ALIGN_CENTER)
                                         
        self.parent = parent.parent
        self.CreateGrid(0, 11)      
        
        self.EnableDragColMove(True)
   
        self.SetColLabelValue(C_NAME, "Name")
        self.SetColLabelValue(C_PATH, "Path")
        self.SetColLabelValue(C_SIZE, "Size")
        self.SetColLabelValue(C_THUMB, "Thumbnail")
        self.SetColLabelValue(C_APL, "Content")
        self.SetColLabelValue(C_ASPECT, "DAR")
        self.SetColLabelValue(C_PROFILE, "Encoding Profile")
        self.SetColLabelValue(C_DEINTERLACE, "De-int")
        self.SetColLabelValue(C_FTP, "Ftp")
        self.SetColLabelValue(C_STATE, "State")
        self.SetColLabelValue(C_ENC, "Encoding")
        
        attr_ro = wx.grid.GridCellAttr()   
        attr_ro.SetReadOnly(True) 
        attr_ro.SetBackgroundColour(wx.Colour(220, 220, 255))         
        self.SetColAttr(C_PATH, attr_ro) 
        self.SetColAttr(C_SIZE, attr_ro)
        
        attr_0 = wx.grid.GridCellAttr()
        attr_0.SetBackgroundColour(wx.Colour(220, 255, 255))       
        self.SetColAttr(C_NAME, attr_0) 
             
        attr_1 = wx.grid.GridCellAttr()
        attr_1.SetEditor(MyCellEditor_Choice(self, APL)) 
        attr_1.SetBackgroundColour(wx.Colour(255, 255, 220))       
        self.SetColAttr(C_APL, attr_1)    
        
        attr_2 = wx.grid.GridCellAttr()
        attr_2.SetEditor(MyCellEditor_Choice(self, ASPECTR))
        attr_2.SetBackgroundColour(wx.Colour(255, 220, 220))        
        self.SetColAttr(C_ASPECT, attr_2)
        
        attr_3 = wx.grid.GridCellAttr()
        if EDITION == 0: attr_3.SetEditor(MyCellEditor_Choice(self, PROFILES))
        elif EDITION == 1: attr_3.SetEditor(MyCellEditor_Choice(self, PROFILES_1))
        attr_3.SetBackgroundColour(wx.Colour(255, 220, 220))
        self.SetColAttr(C_PROFILE, attr_3)
                
        attr_4 = wx.grid.GridCellAttr()
        attr_4.SetEditor(MyCellEditor_Dial(self))
        attr_4.SetBackgroundColour(wx.Colour(255, 255, 220))
        self.SetColAttr(C_THUMB, attr_4)
        
        attr_5 = wx.grid.GridCellAttr()
        if EDITION == 0: attr_5.SetEditor(MyCellEditor_Choice(self, FTPSEND))
        elif EDITION == 1: attr_5.SetEditor(MyCellEditor_Choice(self, FTPSEND_1))
        attr_5.SetBackgroundColour(wx.Colour(0, 150, 0))
        attr_5.SetTextColour(wx.Colour(255, 255, 255))
        self.SetColAttr(C_FTP, attr_5)
        
        attr_6 = wx.grid.GridCellAttr()
        attr_6.SetReadOnly(True)        
        attr_6.SetBackgroundColour(wx.Colour(0, 0, 150))
        attr_6.SetTextColour(wx.Colour(255, 255, 255))
        self.SetColAttr(C_STATE, attr_6)
        
        attr_7 = wx.grid.GridCellAttr()
        attr_7.SetEditor(MyCellEditor_Choice(self, ENCODING))                
        attr_7.SetBackgroundColour(wx.Colour(0, 0, 0))
        attr_7.SetTextColour(wx.Colour(255, 255, 255)) 
        self.SetColAttr(C_ENC, attr_7)

        attr_8 = wx.grid.GridCellAttr()
        attr_8.SetEditor(MyCellEditor_Choice(self, DEINTERLACE))                
        attr_8.SetBackgroundColour(wx.Colour(83, 35, 56))
        attr_8.SetTextColour(wx.Colour(255, 255, 255)) 
        self.SetColAttr(C_DEINTERLACE, attr_8)
                
        self.CalibrateSize()  

        self.Bind(wx.EVT_KEY_DOWN, self.__OnKeyDown)
        
    def AddItem(self, path): 
        import os 
        size = str(os.path.getsize(path) / 1048576) + "MiB"        
        self.SetCellValue(self.GetNumberRows()-1, C_PATH, path)
        self.SetCellValue(self.GetNumberRows()-1, C_SIZE, size)
        self.SetCellValue(self.GetNumberRows()-1, C_THUMB, THUMB[0])
        self.SetCellValue(self.GetNumberRows()-1, C_APL, APL[0])
        self.SetCellValue(self.GetNumberRows()-1, C_ASPECT, ASPECTR[0])
        if EDITION == 0: self.SetCellValue(self.GetNumberRows()-1, C_PROFILE, PROFILES[2])
        elif EDITION == 1: self.SetCellValue(self.GetNumberRows()-1, C_PROFILE, PROFILES[4])
        self.SetCellValue(self.GetNumberRows()-1, C_DEINTERLACE, DEINTERLACE[2])
        name = self.Filter(os.path.splitext(os.path.basename(path))[0], self.GetNumberRows()-1)
        self.SetCellValue(self.GetNumberRows()-1, C_NAME, name)
        self.SetCellValue(self.GetNumberRows()-1, C_STATE, STATES[0])
        self.SetCellValue(self.GetNumberRows()-1, C_FTP, FTPSEND[1])
        self.SetCellValue(self.GetNumberRows()-1, C_ENC, ENCODING[0])
        
        print "INFO: Task [%i] with \"%s\" added" %(self.GetNumberRows()-1, name)
        self.AutoSizeRow(self.GetNumberRows()-1)
                
    def Filter(self, name, row):
        timestring = str(time.localtime()[0]) + \
                    str(time.localtime()[1]).zfill(2) + \
                    str(time.localtime()[2]).zfill(2)
        #search = self.filter.search(name)  
        newname = re.sub(r'_(HD1080p|HD720p|SD576p|YT576p|MQ336p)$', '', name)
        newname = re.sub(r'^EXT_', '', newname)
        newname = re.sub(r'^PREVIEW_', '', newname)
        newname = re.sub(r'^2\d{7}_', '', newname)
        newname = re.sub(r' ', '_', newname)
        if EDITION == 0:
            if self.GetCellValue(row, C_PROFILE) == PROFILES[5]:
                newname = "PREVIEW_" + newname
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[0]:   
                newname =  timestring + "_" + newname + "_HD1080p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[1]:
                newname =  timestring + "_" + newname + "_HD720p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[2]:
                newname =  timestring + "_" + newname + "_SD576p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[3]:
                newname =  timestring + "_" + newname + "_YT576p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[4]:
                newname =  timestring + "_" + newname + "_MQ336p"
            else:
                newname = timestring + "_" + newname
        elif EDITION == 1:
            if self.GetCellValue(row, C_PROFILE) == PROFILES[5]:
                newname = "EXT_PREVIEW_" + newname
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[0]:   
                newname =  "EXT_" + timestring + "_" + newname + "_HD1080p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[1]:
                newname =  "EXT_" + timestring + "_" + newname + "_HD720p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[2]:
                newname =  "EXT_" + timestring + "_" + newname + "_SD576p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[3]:
                newname =  "EXT_" + timestring + "_" + newname + "_YT576p"
            elif self.GetCellValue(row, C_PROFILE) == PROFILES[4]:
                newname =  "EXT_" + timestring + "_" + newname + "_MQ336p"
            else:
                newname = "EXT_" + timestring + "_" + newname
        return newname
            
    def CalibrateSize(self):
        self.AutoSizeColumns()
        cols_num = self.GetNumberCols()
        cols_width = 70
        for x in range(cols_num):
            cols_width += self.GetColSize(x)
        if self.parent.GetSize()[0] > cols_width:
            col_widthToAdd = (self.parent.GetSize()[0] - cols_width) / cols_num            
            for x in range(cols_num):                
                    self.SetColSize(x, self.GetColSize(x) + col_widthToAdd)                 
        elif self.parent.GetSize()[0] <= cols_width:
            self.AutoSizeColumns()
        #self.parent.Layout()
        
    def __OnKeyDown(self, event):
        keycode = event.GetKeyCode()       
        if keycode == wx.WXK_DELETE:
            selections = []
            try:
                start = self.GetSelectionBlockTopLeft()[0][0]
                end = self.GetSelectionBlockBottomRight()[0][0]                 
                self.DeleteRows(start, end-start+1, True) 
            except Exception:               
                selections = self.GetSelectedRows()            
            selections.sort(reverse=True)
            for s in selections:               
                print "INFO: Task [%i] with \"%s\" deleted" % (s, self.GetCellValue(s, C_NAME)) 
                self.DeleteRows(s, 1, True) 
        if event.ControlDown() and keycode == 65:
            self.SelectAll()     
        if event.ControlDown() and keycode == 86:
            do = wx.FileDataObject()
            wx.TheClipboard.Open()
            success = wx.TheClipboard.GetData(do)
            wx.TheClipboard.Close()
            if success:
                import os
                fileList = []
                for filename in do.GetFilenames():
                            fileList.append(os.path.abspath(filename))
                            for root, subFolders, files in os.walk(filename, topdown=False):                
                                for file in files:                    
                                    fileList.append(os.path.join(root,file))
                for path in fileList:  
                    import re            
                    for x in VFORMATSLIST:
                        pattern = r"(.%s$)" % (x)                
                        if re.search(pattern, path, re.I):
                            self.AppendRows(1, True)
                            self.AddItem(path)
                self.CalibrateSize()                         
        event.Skip()

class FileDrop(wx.FileDropTarget):
    def __init__(self, parent):
        wx.FileDropTarget.__init__(self)
        self.parent = parent

    def OnDropFiles(self, x, y, filenames):
        import os
        fileList = []
        for filename in filenames:
            fileList.append(os.path.abspath(filename))
            for root, subFolders, files in os.walk(filename, topdown=False):                
                for file in files:                    
                    fileList.append(os.path.join(root,file))
        for path in fileList:  
            import re            
            for x in VFORMATSLIST:
                pattern = r"(.%s$)" % (x)                
                if re.search(pattern, path, re.I): 
                    self.parent.AppendRows(1, True)
                    self.parent.AddItem(path)
        self.parent.CalibrateSize()
        
class MyCellEditor_Choice(wx.grid.PyGridCellEditor):
    def __init__(self, parent, choiceList): 
        self.parent = parent
        self.choiceList = choiceList       
        wx.grid.PyGridCellEditor.__init__(self)

    def Create(self, parent, id, evtHandler):
        self._tc = wx.Choice(parent, id, (100, 50), choices = self.choiceList)
        self.SetControl(self._tc)
        if evtHandler:
            self._tc.PushEventHandler(evtHandler)

    def SetSize(self, rect):        
        self._tc.SetDimensions(rect.x, rect.y, rect.width+2, rect.height+2, wx.SIZE_ALLOW_MINUS_ONE)

    def Show(self, show, attr): 
        wx.grid.PyGridCellEditor.Show(self, show, attr)    
        #self.base_Show(show, attr)
        #self.Show(show, attr)
        
    def PaintBackground(self, rect, attr):
        pass

    def BeginEdit(self, row, col, grid): 
        self._tc.SetFocus()       
        self.startValue = grid.GetTable().GetValue(row, col)
        self._tc.SetStringSelection(self.startValue)        

    def EndEdit(self, row, col, grid): 
        ## general       
        changed = False

        val = self._tc.GetStringSelection()
        if val != self.startValue:
            changed = True
            grid.GetTable().SetValue(row, col, val)

        self.startValue = self.choiceList[0]
        self._tc.SetStringSelection(self.choiceList[0])
        
        ## specified
        if col == C_PROFILE:            
            newname = grid.Filter(grid.GetTable().GetValue(row, C_NAME), row)   
            grid.GetTable().SetValue(row, C_NAME, newname)
            grid.Refresh()
        elif col == C_ENC:
            if ENCODING[0] == grid.GetTable().GetValue(row, C_ENC):
                grid.SetCellTextColour(row, C_APL, wx.Colour(0, 0, 0))
                grid.SetCellTextColour(row, C_PROFILE, wx.Colour(0, 0, 0))
                grid.SetCellTextColour(row, C_THUMB, wx.Colour(0, 0, 0)) 
                grid.SetReadOnly(row, C_APL, False)
                grid.SetReadOnly(row, C_PROFILE, False)
                grid.SetReadOnly(row, C_THUMB, False)   
            else:
                grid.SetCellTextColour(row, C_APL, wx.Colour(200, 200, 200))
                grid.SetCellTextColour(row, C_PROFILE, wx.Colour(200, 200, 200))
                grid.SetCellTextColour(row, C_THUMB, wx.Colour(200, 200, 200))
                grid.SetReadOnly(row, C_APL, True)
                grid.SetReadOnly(row, C_PROFILE, True)
                grid.SetReadOnly(row, C_THUMB, True)
            grid.Refresh()
        return changed

    def Reset(self):        
        self._tc.SetStringSelection(self.startValue)
        
    def StartingClick(self):
        pass

    def Destroy(self):        
        wx.grid.PyGridCellEditor.Destroy(self)

    def Clone(self):       
        return MyCellEditor_Choice(self.log)

class MyCellEditor_Dial(wx.grid.PyGridCellEditor):
    def __init__(self, parent): 
        self.parent = parent              
        wx.grid.PyGridCellEditor.__init__(self)

    def Create(self, parent, id, evtHandler):
        self._tc = wx.Button(parent=parent, id=id, size=(100, 50), label="set path")
        self.SetControl(self._tc)        
        if evtHandler:
            self._tc.PushEventHandler(evtHandler)

    def SetSize(self, rect):        
        self._tc.SetDimensions(rect.x, rect.y, rect.width+2, rect.height+2, wx.SIZE_ALLOW_MINUS_ONE)

    def Show(self, show, attr): 
        wx.grid.PyGridCellEditor.Show(self, show, attr)    
        #self.base_Show(show, attr)
        #self.Show(show, attr)

    def PaintBackground(self, rect, attr):
        pass

    def BeginEdit(self, row, col, grid): 
        self._tc.SetFocus()        
        dialog = wx.FileDialog (None, message='Open video files to add to list...', wildcard=GFORMATS, style=wx.OPEN)
        if dialog.ShowModal() == wx.ID_OK:            
            selected = dialog.GetPath() 
            temp = wx.Bitmap(selected)
            if temp.GetSize()[0] != 100 and temp.GetSize()[1] != 75:
                print "INFO: Thumbnail size: %ix%i" % (temp.GetSize()[0], temp.GetSize()[1])
                import sys
                sys.stderr.write("ERR: Wrong thumbnail size - it should be 100x75\n")
                grid.GetTable().SetValue(row, col, "no thumb")
                return
            import os   
            grid.GetTable().SetValue(row, col, os.path.normcase(selected)) 
            self.parent.CalibrateSize()
            self.parent.Layout()              
        dialog.Destroy()
        
    def EndEdit(self, row, col, grid):        
        pass

    def Reset(self):        
        pass
        
    def StartingClick(self):
        pass

    def Destroy(self):        
        self.base_Destroy()

    def Clone(self):       
        return MyCellEditor_Dial(self.log)
